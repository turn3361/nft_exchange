import { combineReducers, Reducer, AnyAction } from 'redux';
import { RootStateInterface } from '../interfaces/RootState';
import userInfo from './userInfo';

const rootReducer: Reducer<RootStateInterface, AnyAction> =
    combineReducers<RootStateInterface>({
        userInfo,
    });

export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>;
