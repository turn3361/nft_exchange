import { UserInfo, ActionProps } from '../interfaces/user/UserInfo.interface';

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOOUT';

export interface LoginAction {
    type: typeof LOGIN;
}
export interface LogoutAction {
    type: typeof LOGOUT;
}
const initialState: UserInfo = { isLogin: false };

const loginReducer = (state = initialState, action: ActionProps) => {
    switch (action.type) {
        case LOGIN:
            return { ...state, isLogin: true };
        case LOGOUT:
            return { ...state, isLogin: false };
        default:
            return state;
    }
};
export default loginReducer;
