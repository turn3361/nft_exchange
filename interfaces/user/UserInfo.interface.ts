export interface ActionProps {
    type: string;
}

export interface UserInfo {
    isLogin: boolean;
}
