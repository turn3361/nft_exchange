export interface UserPayload {
    SUCCES: string;
    FAILURE: string;
    ERROR: string;
}
