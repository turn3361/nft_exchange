import { UserInfo } from './user/UserInfo.interface';

export interface RootStateInterface {
    userInfo: UserInfo;
}
