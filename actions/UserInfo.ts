import { ActionProps } from '../interfaces/user/UserInfo.interface';
import { LOGIN, LOGOUT } from '../reducers/userInfo';

export function login(): ActionProps {
    return { type: LOGIN };
}
export function logout(): ActionProps {
    return { type: LOGOUT };
}
