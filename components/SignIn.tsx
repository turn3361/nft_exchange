import { NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';
import Footer from './layout/Footer';
const SignInContainer = styled.div`
    width: 68%;
    margin: 0 auto;
    padding: 50px 0 200px;
    text-align: center;
    h1 {
        font-size: 1.8rem;
        margin: 1rem auto 2rem;
    }
    div {
        text-align: center;

        img {
            width: 150px;
            margin: 0 auto 80px;
            display: block;
        }
        a {
            border: 1px solid #e8632e;
            background-color: #e8632e;
            color: #fff;
            padding: 0.5rem 1rem;
            font-size: 1.25rem;
            border-radius: 0.3rem;
        }
    }
`;
const SignIn: NextPage = () => {
    return (
        <>
            <SignInContainer>
                <h1>Sign in to your wallet.</h1>
                <div>
                    <img src="https://nftmania.io/views/_layout/bootstrap/images/metamask.png" />
                    <a href="https://metamask.app.link/dapp/nftmania.io/frame">
                        Sign In
                    </a>
                </div>
            </SignInContainer>
            <Footer />
        </>
    );
};
export default SignIn;
