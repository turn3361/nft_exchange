import React from 'react';
import styled from 'styled-components';
const BodyContainer = styled.div`
    width: 100%;
    display: block;
    min-width: 0;
    flex: 1 0 0%;
`;
const BodyItemContainer = styled.div`
    display: flex;
    padding: 32px;
    flex-direction: column;
`;
const BodyItemViewContainer = styled.div`
    display: flex;
    justify-content: space-between;
    div {
        padding: 20px;
        flex-basis: 49.5%;
        border: 1px solid rgb(229, 232, 235);
        border-radius: 5px 5px 0 0;
        border-bottom: 0;
        font-size: 18px;
        cursor: pointer;
    }
`;
const BodyItemViewOfferContainer = styled.div`
    width: 100%;
    border: 1px solid rgb(229, 232, 235);
    border-radius: 0 0 5px 5px;
    overflow: hidden;
    height: calc(100vh - 202px);
`;
const BodyItemListContainer = styled.div`
    max-height: none;
    height: 100%;
    background-color: rgb(251, 253, 255);
    width: 100%;
    overflow-x: auto;
    display: flex;
    flex-direction: column;
    overflow-y: auto;
`;

const BodyItemListTableContainer = styled.div`
    max-height: none;
    height: 100%;
    background-color: rgb(251, 253, 255);
    overflow-x: auto;
    display: flex;
    flex-direction: column;
    overflow-y: auto;
    width: 100%;
`;
const BodyItemListTableHeader = styled.div`
    position: sticky;
    top: 0;
    z-index: 1;
    display: flex;
`;
const BodyItemListTableHeaderCategory = styled.div`
    flex-basis: 150px;
    background-color: #fff;
    color: rgb(14, 14, 14);
    padding: 4px 4px 4px 16px;
    align-items: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    display: flex;
    flex: 1 0 140px;
    overflow-x: auto;
`;
const BodyItemListTableHeaderCategoryItem = styled(
    BodyItemListTableHeaderCategory,
)`
    flex-basis: 300px;
`;
const BodyItemListTableHeaderCategoryQuantity = styled(
    BodyItemListTableHeaderCategory,
)`
    flex-basis: 80px;
`;

const Offer = () => {
    return (
        <BodyContainer>
            <BodyItemContainer>
                <BodyItemViewContainer>
                    <div>Offers Made</div>
                    <div>Offers Received</div>
                </BodyItemViewContainer>
                <BodyItemViewOfferContainer>
                    <BodyItemListContainer>
                        <BodyItemListTableContainer>
                            <BodyItemListTableHeader>
                                <BodyItemListTableHeaderCategoryItem>
                                    item
                                </BodyItemListTableHeaderCategoryItem>
                                <BodyItemListTableHeaderCategory>
                                    Unit Price
                                </BodyItemListTableHeaderCategory>
                                <BodyItemListTableHeaderCategoryQuantity>
                                    Quantity
                                </BodyItemListTableHeaderCategoryQuantity>
                                <BodyItemListTableHeaderCategory>
                                    From
                                </BodyItemListTableHeaderCategory>
                                <BodyItemListTableHeaderCategory>
                                    To
                                </BodyItemListTableHeaderCategory>
                                <BodyItemListTableHeaderCategory>
                                    Date
                                </BodyItemListTableHeaderCategory>
                            </BodyItemListTableHeader>
                        </BodyItemListTableContainer>
                    </BodyItemListContainer>
                </BodyItemViewOfferContainer>
            </BodyItemContainer>
        </BodyContainer>
    );
};
export default Offer;
