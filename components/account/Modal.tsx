import React from 'react';
import styled from 'styled-components';
import Modal from 'react-modal';
import { BsList } from 'react-icons/bs';
import { AiOutlineClose } from 'react-icons/ai';

const ModalHeader = styled.div`
    display: flex;
    flex-shrink: 0;
    justify-content: space-between;
    padding: 1rem 1rem 1rem 0.5rem;
    border-bottom: 1px solid #dee2e6;
    width: 100%;
    h2 {
        margin-bottom: 0;
        line-height: 1.5;
        font-size: 1.25rem;
        font-weight: bold;
    }
    svg {
        padding: 0.5rem 0.5rem;
        margin: -0.5rem -0.5rem -0.5rem auto;
        cursor: pointer;
        box-sizing: content-box;
        color: #000;
        border: 0;
        border-radius: 0.25rem;
        opacity: 0.5;
    }
`;
const ModalBodyContainer = styled.div`
    padding: 20px 5% 0;
    position: relative;
    flex: 1 1 auto;
    h3 {
        text-align: center;
        font-size: 0.95rem;
        margin-bottom: 20px;
        color: #333;
    }
    label {
        margin-bottom: 0.5rem;
        display: inline-block;
    }
`;
const ModalBodyInputContainer = styled.div`
    margin-bottom: 1rem;
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: stretch;
    width: 100%;
    input {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        display: block;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        background-color: #e9ecef;
        opacity: 1;
        position: relative;
        flex: 1 1 auto;
        width: 1%;
        min-width: 0;
    }
    button {
        margin-left: -1px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border: 1px solid #e8632e;
        color: #e8632e;
        position: relative;
        z-index: 2;
        display: inline-block;
        font-weight: 400;
        line-height: 1.5;
        text-align: center;
        text-decoration: none;
        vertical-align: middle;
        background-color: transparent;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        border-radius: 0.25rem;
        cursor: pointer;
    }
`;
const ModalSalesContainer = styled.div`
    height: 350px;
    border-radius: 5px;
    overflow: hidden;
    margin-bottom: 25px;
`;
const ModalSalesListHeaderContainer = styled.div`
    padding: 20px 0 20px 20px;
    border-bottom: 1px solid rgb(229, 232, 235);
    font-size: 18px;
    font-weight: bold;
    display: flex;
    align-items: center;
    svg {
        margin-right: 10px;
    }
    h3 {
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 0;
    }
`;
const ModalSalesListBodyContainer = styled.div`
    width: 100%;
    overflow-x: auto;
    display: flex;
    flex-direction: column;
    overflow-y: auto;
`;
const ModalSalesListBodyTableHeaderContainer = styled.div`
    position: sticky;
    top: 0;
    z-index: 1;
    display: flex;
    overflow: hidden;
    div {
        padding-left: 16px;
        background-color: #fff;
        color: rgb(14, 14, 14);
        padding: 4px;
        align-items: center;
        border-bottom: 1px solid rgb(229, 232, 235);
        display: flex;
        flex: 1 0 140px;
        overflow-x: auto;
    }
`;
const ModalFooter = styled.div`
    display: flex;
    flex-wrap: wrap;
    flex-shrink: 0;
    align-items: center;
    justify-content: flex-end;
    padding: 0.75rem;
    border-top: 1px solid #dee2e6;
    border-bottom-right-radius: calc(0.3rem - 1px);
    border-bottom-left-radius: calc(0.3rem - 1px);
    button {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
        display: inline-block;
        font-weight: 400;
        line-height: 1.5;
        text-align: center;
        text-decoration: none;
        vertical-align: middle;
        border: 1px solid transparent;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        border-radius: 0.25rem;
        cursor: pointer;
    }
`;
type ModalState = {
    modalIsOpen: boolean;
    setModalIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
};
const ModalComponent = (props: ModalState) => {
    const { modalIsOpen, setModalIsOpen } = props;
    return (
        <Modal
            isOpen={modalIsOpen}
            contentLabel="Example Modal"
            style={{
                content: {
                    display: 'flex',
                    width: 800,
                    height: 700,
                    margin: '5rem auto',
                    flexDirection: 'column',
                    justifyContent: 'center',
                },
                overlay: {
                    zIndex: 1000,
                    background: 'rgba(100, 100, 100, 0.8)',
                },
            }}
            ariaHideApp={false}
        >
            <ModalHeader>
                <h2>Refer a friend to NFTMANIA</h2>
                <AiOutlineClose
                    size="32"
                    onClick={() => {
                        setModalIsOpen(false);
                    }}
                />
            </ModalHeader>
            <ModalBodyContainer>
                <h3>
                    Copy your unique referral link and share it far and wide.
                    Any time a new user buys something on
                    <br /> NFTMANIA, you’ll earn at least 1% of the sale!
                    Referrals are processed every two weeks. Due to high
                    <br />
                    gas prices, only referrals earning over .005 ETH will be
                    processed.
                </h3>
                <label>Referral Link</label>
                <ModalBodyInputContainer>
                    <input
                        type="text"
                        defaultValue="https://nftmania.io/recommend/0x7a92d4b067aacd8f85d00a7776290a343a2a5c28"
                    />
                    <button>Copy</button>
                </ModalBodyInputContainer>
                <ModalSalesContainer>
                    <ModalSalesListHeaderContainer>
                        <BsList size="25" />
                        <h3>Referred Sales</h3>
                    </ModalSalesListHeaderContainer>
                    <ModalSalesListBodyContainer>
                        <ModalSalesListBodyTableHeaderContainer>
                            <div>User</div>
                            <div>Item</div>
                            <div>Date</div>
                            <div>Sale Price</div>
                            <div>Refer Point</div>
                        </ModalSalesListBodyTableHeaderContainer>
                    </ModalSalesListBodyContainer>
                </ModalSalesContainer>
            </ModalBodyContainer>
            <ModalFooter>
                <button
                    onClick={() => {
                        setModalIsOpen(false);
                    }}
                >
                    Close
                </button>
            </ModalFooter>
        </Modal>
    );
};

export default ModalComponent;
