import React from 'react';
import { BiShuffle } from 'react-icons/bi';
import styled from 'styled-components';
const BodyContainer = styled.form`
    margin: 0;
    padding: 0;
    display: block;
    margin-block-end: 1em;
`;
const BodyItemContainer = styled.div`
    display: flex;
    background: white;
    flex-direction: row-reverse;
`;
const BodyItemViewContainer = styled.div`
    width: 100%;
    display: block;
    min-width: 0;
    flex: 1 0 0%;
`;
const BodyItemViewTradingContainer = styled.div`
    padding: 32px;
`;
const BodyItemListContainer = styled.div`
    border: 1px solid rgb(229, 232, 235);
    border-radius: 5px;
    overflow: hidden;
    height: calc(100vh - 303px);
`;
const BodyItemListHeader = styled.div`
    padding: 20px 0 20px 20px;
    border-bottom: 1px solid rgb(229, 232, 235);
    font-size: 18px;
    font-weight: bold;
    display: flex;
    align-items: center;
    svg {
        margin-right: 10px;
    }
`;
const BodyItemListTableContainer = styled.div`
    max-height: none;
    height: 100%;
    background-color: rgb(251, 253, 255);
    overflow-x: auto;
    display: flex;
    flex-direction: column;
    overflow-y: auto;
    width: 100%;
`;
const BodyItemListTableHeader = styled.div`
    position: sticky;
    top: 0;
    z-index: 1;
    display: flex;
`;
const BodyItemListTableHeaderCategory = styled.div`
    flex-basis: 150px;
    background-color: #fff;
    color: rgb(14, 14, 14);
    padding: 4px 4px 4px 16px;
    align-items: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    display: flex;
    flex: 1 0 140px;
    overflow-x: auto;
`;
const BodyItemListTableHeaderCategoryItem = styled(
    BodyItemListTableHeaderCategory,
)`
    flex-basis: 300px;
`;
const BodyItemListTableHeaderCategoryQuantity = styled(
    BodyItemListTableHeaderCategory,
)`
    flex-basis: 80px;
`;
const Activity = () => {
    return (
        <BodyContainer>
            <BodyItemContainer>
                <BodyItemViewContainer>
                    <BodyItemViewTradingContainer>
                        <BodyItemListContainer>
                            <BodyItemListHeader>
                                <BiShuffle size="25" />
                                <p>Trading History</p>
                            </BodyItemListHeader>
                            <BodyItemListTableContainer>
                                <BodyItemListTableHeader>
                                    <BodyItemListTableHeaderCategory>
                                        Event
                                    </BodyItemListTableHeaderCategory>
                                    <BodyItemListTableHeaderCategoryItem>
                                        item
                                    </BodyItemListTableHeaderCategoryItem>
                                    <BodyItemListTableHeaderCategory>
                                        Unit Price
                                    </BodyItemListTableHeaderCategory>
                                    <BodyItemListTableHeaderCategoryQuantity>
                                        Quantity
                                    </BodyItemListTableHeaderCategoryQuantity>
                                    <BodyItemListTableHeaderCategory>
                                        From
                                    </BodyItemListTableHeaderCategory>
                                    <BodyItemListTableHeaderCategory>
                                        To
                                    </BodyItemListTableHeaderCategory>
                                    <BodyItemListTableHeaderCategory>
                                        Date
                                    </BodyItemListTableHeaderCategory>
                                </BodyItemListTableHeader>
                            </BodyItemListTableContainer>
                        </BodyItemListContainer>
                    </BodyItemViewTradingContainer>
                </BodyItemViewContainer>
            </BodyItemContainer>
        </BodyContainer>
    );
};

export default Activity;
