import React from 'react';
import styled from 'styled-components';
const BodyContainer = styled.div`
    display: flex;
    background: white;
    flex-direction: row-reverse;
`;
const BodyViewContainer = styled.div`
    width: 100%;
    display: block;
    min-width: 0;
    flex: 1 0 0%;
`;
const BodyViewListContainer = styled.div`
    padding: 32px;
    flex-direction: column;
    display: flex;
`;
const BodyListContainer = styled.div`
    width: 100%;
    border: 1px solid rgb(229, 232, 235);
    border-radius: 0 0 5px 5px;
    overflow: hidden;
    height: calc(100vh - 202px);
`;
const BodyListTableContainer = styled.div`
    max-height: none;
    height: 100%;
    background-color: rgb(251, 253, 255);
    width: 100%;
    overflow-x: auto;
    display: flex;
    flex-direction: column;
    overflow-y: auto;
`;
const BodyTableHeaderContainer = styled.div`
    position: sticky;
    top: 0;
    z-index: 1;
    display: flex;
`;
const BodyTavleHeaderCell = styled.div`
    flex-basis: 150px;
    background-color: #fff;
    color: rgb(14, 14, 14);
    padding: 4px 4px 4px 16px;
    align-items: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    display: flex;
    flex: 1 0 140px;
    overflow-x: auto;
`;
const BodyTavleHeaderCellImg = styled(BodyTavleHeaderCell)`
    flex-basis: 150px;
`;

const Event = () => {
    return (
        <BodyContainer>
            <BodyViewContainer>
                <BodyViewListContainer>
                    <BodyListContainer>
                        <BodyListTableContainer>
                            <BodyTableHeaderContainer>
                                <BodyTavleHeaderCell>
                                    Event Name
                                </BodyTavleHeaderCell>
                                <BodyTavleHeaderCellImg>
                                    Participation Content
                                </BodyTavleHeaderCellImg>
                                <BodyTavleHeaderCell>
                                    Event Status
                                </BodyTavleHeaderCell>
                                <BodyTavleHeaderCell>
                                    Date of Participation
                                </BodyTavleHeaderCell>
                            </BodyTableHeaderContainer>
                        </BodyListTableContainer>
                    </BodyListContainer>
                </BodyViewListContainer>
            </BodyViewContainer>
        </BodyContainer>
    );
};
export default Event;
