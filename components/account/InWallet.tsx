import React from 'react';
import { BiSearchAlt2 } from 'react-icons/bi';
import styled from 'styled-components';

const BodyContainer = styled.div`
    display: flex;
    background: white;
    flex-direction: row-reverse;
`;
const BodySearchContainer = styled.div`
    width: 100%;
    display: block;
    padding-bottom: 80px;
    min-width: 0;
    flex: 1 0 0%;
`;
const BodySearchInputContainer = styled.div`
    display: flex;
    padding: 0 32px;
    flex-flow: row wrap;
    -webkit-box-pack: justify;
    justify-content: space-between;
    margin-bottom: 4px;
    max-width: calc(88vw);
    div {
        display: flex;
        flex: 1 0 0%;
        align-items: center;
        margin: 16px 0 0;
        padding-right: 20px;
        position: relative;
        flex-wrap: wrap;
        width: 100%;
        span {
            padding: 0.61rem 0.75rem;
            display: flex;
            align-items: center;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: center;
            white-space: nowrap;
            background-color: #e9ecef;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            svg {
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                text-align: center;
                white-space: nowrap;
            }
        }
        input {
            display: block;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            appearance: none;
            border-radius: 0.25rem;
            position: relative;
            flex: 1 1 auto;
            width: 1%;
            min-width: 0;
        }
    }
`;
const InWallet = () => {
    return (
        <BodyContainer>
            <form>
                <input type="hidden" />
            </form>
            <BodySearchContainer>
                <BodySearchInputContainer>
                    <div>
                        <span>
                            <BiSearchAlt2 />
                        </span>
                        <input type="text" placeholder="search" />
                    </div>
                </BodySearchInputContainer>
            </BodySearchContainer>
        </BodyContainer>
    );
};

export default InWallet;
