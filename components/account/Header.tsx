import Link from 'next/link';
import React from 'react';
import { AiFillMeh } from 'react-icons/ai';
import { BiCalendarCheck, BiPencil } from 'react-icons/bi';
import {
    BsClockHistory,
    BsFillBookmarksFill,
    BsGear,
    BsHeart,
} from 'react-icons/bs';
import { FaProjectDiagram } from 'react-icons/fa';
import styled from 'styled-components';
const HeaderContainer = styled.div`
    display: flex;
    flex-direction: column;
`;
const HeaderBannerContainer = styled.div`
    height: 225px;
    display: flex;
    min-height: 120px;
    overflow: hidden;
    position: relative;
    background: rgb(229, 232, 235);
    justify-content: center;
    align-items: center;
    img {
        object-fit: cover;
        height: 100%;
        width: 100%;
        vertical-align: middle;
    }
    div {
        position: absolute;
        top: 15px;
        right: 15px;
        width: 50px;
        height: 50px;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: #fff;
        border-radius: 5px;
        label {
            width: 100%;
            height: 100%;
            z-index: 1;
            cursor: pointer;
            position: absolute;
            top: 0;
            left: 0;
            display: inline-block;
        }
        svg {
        }
        input {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }
    }
`;
const HeaderUserInfoContainer = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-top: -64px;
    h2 {
        font-size: 32px;
        font-weight: 600;
        min-height: 40px;
        margin-top: 4px;
        text-align: center;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }
    a {
        display: flex;
        align-items: center;
        min-height: 24px;
        color: rgb(138, 147, 155);
        font-size: 16px;
        text-decoration: none;
    }
    h3 {
        display: flex;
        align-items: center;
        min-height: 24px;
        color: rgb(138, 147, 155);
        font-size: 16px;
        margin-top: 6px;
        max-width: 900px;
        text-align: center;
    }
`;
const HeaerUserImgContainer = styled.div`
    background: #fff;
    border-radius: 50%;
    width: 130px;
    height: 130px;
    position: relative;
    cursor: pointer;
    img {
        width: 130px;
        height: 130px;
        object-fit: cover;
        overflow: hidden;
        border-radius: 50%;
    }
    label {
        display: none;
        position: absolute;
        cursor: pointer;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 100%;
        justify-content: center;
        align-items: flex-end;
        span {
            width: 100%;
            height: 24px;
            line-height: 22px;
            background: rgba(0, 0, 0, 0.3);
            text-align: center;
            color: #fff;
            font-size: 14px;
            letter-spacing: 1px;
        }
        input {
            display: none;
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }
    }
    &:hover {
        label {
            display: flex;
        }
    }
`;
const HeaderUserInfoBtnContainer = styled.div`
    position: absolute;
    top: 79px;
    right: 15px;
    z-index: 2;
    a {
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 5px;
        border: 1px solid rgb(229, 232, 235);
        width: 50px;
        height: 50px;
        flex-direction: row;
        color: rgb(138, 147, 155);
        font-size: 14px;
        text-decoration: none;
    }
`;
const HeaderNavbarContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    background-color: #fff;
    border-bottom: 1px solid rgb(229, 232, 235);
    padding: 12px 0;
    position: sticky;
    z-index: 2;

    svg {
        margin-right: 8px;
    }
`;
const NonSelectHeaderNavbar = styled.div`
    background: #fff;
    display: flex;
    align-items: center;
    padding: 8px 16px;
    margin: 0 12px;
    transition: all 0.3s ease 0s;
    border-radius: 4px;
    color: rgb(138, 147, 155);
    font-size: 15px;
    text-decoration: none;
    &:hover {
        background: rgb(229, 232, 235);
        color: rgb(53, 56, 64);
    }
`;
const SelectHeaderNavbar = styled(NonSelectHeaderNavbar)`
    background: rgb(229, 232, 235);
    color: rgb(53, 56, 64);
`;
type ModalState = {
    setModalIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
    selectNav: string | string[] | undefined;
};
const Header = (props: ModalState) => {
    const { setModalIsOpen, selectNav } = props;
    return (
        <HeaderContainer>
            <HeaderBannerContainer>
                <img src="https://nftmania.io/views/_layout/bootstrap/images/sample/bg_sample9.png" />
                <div>
                    <label />
                    <BiPencil size="25" color={'gray'} />
                    <input type="file" />
                </div>
            </HeaderBannerContainer>
            <HeaderUserInfoContainer>
                <HeaerUserImgContainer>
                    <img src="https://nftmania.io/views/_layout/bootstrap/images/user-none.png" />
                    <label>
                        <span>EDIT</span>
                        <input type="file" />
                    </label>
                </HeaerUserImgContainer>
                <h2>60CC3B0A429EF</h2>
                <a href="#">0x7a92d4b067aacd8f85d00a7776290a343a2a5c28</a>
                <h3></h3>
                <HeaderUserInfoBtnContainer>
                    <a href="/accountsettings">
                        <BsGear size="24" color={'gray'} />
                    </a>
                </HeaderUserInfoBtnContainer>
            </HeaderUserInfoContainer>
            <HeaderNavbarContainer>
                <SelectHeaderNavbar>
                    <BsFillBookmarksFill size="20" />
                    <Link
                        href={{
                            pathname: '/account',
                            query: { routeName: 'InWallet' },
                        }}
                        as="/account"
                    >
                        In Wallet
                    </Link>
                </SelectHeaderNavbar>
                <NonSelectHeaderNavbar>
                    <BsClockHistory size="20" />
                    <Link
                        href={{
                            pathname: '/account',
                            query: {
                                routeName: 'activity',
                            },
                        }}
                        as="/accountactivity"
                    >
                        Activity
                    </Link>
                </NonSelectHeaderNavbar>
                <NonSelectHeaderNavbar>
                    <AiFillMeh size="20" />
                    <Link
                        href={{
                            pathname: '/account',
                            query: { routeName: 'offer' },
                        }}
                        as="/accountoffer"
                    >
                        Offers
                    </Link>
                </NonSelectHeaderNavbar>
                <NonSelectHeaderNavbar>
                    <BiCalendarCheck size="20" />
                    <Link
                        href={{
                            pathname: '/account',
                            query: { routeName: 'event' },
                        }}
                        as="/accountevent"
                    >
                        Event
                    </Link>
                </NonSelectHeaderNavbar>
                <NonSelectHeaderNavbar>
                    <BsHeart size="20" />
                    <Link
                        href={{
                            pathname: '/account',
                            query: { routeName: 'favorites' },
                        }}
                        as="/accountfavorites"
                    >
                        Favorites
                    </Link>
                </NonSelectHeaderNavbar>
                <NonSelectHeaderNavbar onClick={() => setModalIsOpen(true)}>
                    <FaProjectDiagram size="20" />
                    <Link
                        href={{
                            pathname: '/account',
                            query: { routeName: 'Referrals' },
                        }}
                        as="/account"
                    >
                        Referrals
                    </Link>
                </NonSelectHeaderNavbar>
            </HeaderNavbarContainer>
        </HeaderContainer>
    );
};
export default Header;
