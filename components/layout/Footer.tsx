import React from 'react';
import styled from 'styled-components';

const Footercontainer = styled.div`
    margin-top: 50px;
    background-color: #de7e58;
    color: rgba(255, 255, 255, 0.8);
`;

const FooterContactcontainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    margin: 0 auto;
    width: 80%;
    img {
        width: 180px;
        height: 57px;
    }
`;

const FooterContactLeftTextcontainer = styled.div`
    margin-top: 40px;
    font-size: 15px;
    color: rgba(255, 255, 255, 0.8);
    font-weight: 300;

    h6 {
        font-weight: 600;
        margin-bottom: 5px;
    }
`;

const FooterContactLeftTextLinkcontainer = styled.div`
    margin: 10px auto;
    display: flex;
    align-items: center;
    a {
        font-size: 12px;
        font-weight: 400;
    }

    div {
        content: ' ';
        margin-left: 20px;
        margin-right: 20px;
        height: 8px;
        border-right: 1px solid #fff;
    }
`;

const FooterContactLeftTextCopycontainer = styled.div`
    font-size: 12px;

    button {
        margin-left: 5px;
        border-radius: 5px;
        color: #de7e58;
        background-color: white;
    }
`;

const FooterContactUnderLine = styled.div`
    width: 83%;
    content: ' ';
    margin: 20px auto 15px auto;
    border-bottom: 1px solid rgba(255, 255, 255, 0.3);
`;
const FooterCopyrightcontainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 80%;
    margin: 0 auto;
    font-size: 0.8rem;
    font-weight: 500;
`;

const FooterCopyrightLinkcontainer = styled.div`
    display: flex;
    flex-direction: row;
    padding-bottom: 15px;
`;

const FooterCopyrightLink = styled.a`
    margin-left: 10px;
`;
const Footer = () => {
    return (
        <Footercontainer>
            <FooterContactcontainer>
                <FooterContactLeftTextcontainer>
                    <h6>CONTACT</h6>
                    <div>
                        Vistra Corporate Services Centre, Wickhams Cay II, Road
                        Town, Tortola, VG1110, British Virgin Islands
                    </div>
                    <br />
                    <div>Email : info@rushcoin.io</div>
                    <br />
                    <FooterContactLeftTextLinkcontainer>
                        <a href="#">How to Register & Create Collection</a>
                        <div />
                        <a href="#">How to Create</a>
                        <div />
                        <a href="#">NFT NFTMANIA YOUTUBE</a>
                    </FooterContactLeftTextLinkcontainer>
                    <br />
                    <FooterContactLeftTextCopycontainer>
                        NFTMANIA Contract :
                        0x694252712E0e12F526A3f692BD2A602D568B8b02
                        <button>Copy</button>
                    </FooterContactLeftTextCopycontainer>
                    <br />
                    <FooterContactLeftTextCopycontainer>
                        RUSH Contract :
                        0x382A1667C9062F0621362F49076Ef6e4fE4C9eC7
                        <button>Copy</button>
                    </FooterContactLeftTextCopycontainer>
                </FooterContactLeftTextcontainer>
                <img src="https://nftmania.io/views/_layout/bootstrap/images/nftmania-logo-footer.png" />
            </FooterContactcontainer>
            <FooterContactUnderLine />
            <FooterCopyrightcontainer>
                <div>
                    Copyright © 2021 Gamechain Co.,LTD. All rights reserved.
                </div>
                <FooterCopyrightLinkcontainer
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                        paddingBottom: 15,
                    }}
                >
                    <a href="#">Privacy Policy</a>
                    <FooterCopyrightLink href="#">
                        Terms of Service
                    </FooterCopyrightLink>
                </FooterCopyrightLinkcontainer>
            </FooterCopyrightcontainer>
        </Footercontainer>
    );
};

export default Footer;
