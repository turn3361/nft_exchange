import React, { useState } from 'react';
import Link from 'next/link';

import { IoPersonCircleOutline } from 'react-icons/io5';
import { BsPersonSquare, BsToggles, BsGear, BsBucket } from 'react-icons/bs';
import { BiSearch, BiCollection } from 'react-icons/bi';
import styled from 'styled-components';

// 스타일드 컴포넌트 선언
const HeaderLayout = styled.div`
    background-color: #f1f1f1;
    position: sticky;
    top: 0;
    z-index: 3;
`;

const Container = styled.div`
    margin: 0 auto;
    width: 67%;
    display: flex;
    flex-direction: column;
`;

const LogoContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    margin-top: 28px;
    margin-bottom: 13px;
    img {
        height: 70px;
        margin-left: 10px;
    }
`;

const LogoImgContainer = styled.div`
    position: relative;
    img:hover {
        cursor: pointer;
    }
`;

const MenuContainer = styled.li`
    list-style: none;
    margin-right: 15px;
    position: relative;
    border: 2px solid transparent;
    padding: 0 10px 4px 10px;
    ul {
        position: absolute;
        right: 0;
        background-color: white;
        width: 216px;
        margin-top: '18px';
        box-shadow: rgb(47 63 78 / 25%) 0px 0px 8px 0px;
        li {
            padding: 13px 15px;
            font-size: 1rem;
            &:hover {
                color: #0d6efd;
                cursor: pointer;
            }
        }
    }

    &:hover {
        cursor: pointer;
        border-bottom: '2px solid red';
    }
`;

const SearchNavbarContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 45px;
    margin-bottom: 15px;
`;

const SearchContainer = styled.div`
    background-color: white;
    display: flex;
    flex: 1.8;
    border-radius: 6px;
    height: 43px;
    align-items: center;
    border: 1px solid #e5e8eb;
    form {
        width: 100%;
    }

    label {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-left: 10px;
    }

    input {
        width: 100%;
        height: 43px;
        font-size: 1rem;
        margin-left: 5px;
    }
`;

const NavbarContainer = styled.ul`
    list-style: none;
    color: rgba(4, 17, 29, 0.75);
    margin: 10px;
    border: 2px solid transparent;
    li {
        float: left;
        padding: 10px 15px;
        border: 2px solid transparent;
        &:hover {
            color: #250b00;
            cursor: pointer;
            border-bottom: 2px solid red;
        }
    }
`;

const NavbarDropdown = styled.li`
    list-style: none;
    margin-right: 15px;
    position: relative;
    border: 2px solid transparent;
    padding: 0 10px 4px 10px;
    ul {
        position: absolute;
        right: 0;
        background-color: white;
        width: 216px;
        margin-top: 13px;
        box-shadow: rgb(47 63 78 / 25%) 0px 0px 8px 0px;
        li {
            padding: 13px 15px;
            font-size: 1rem;
            &:hover {
                color: #0d6efd;
                cursor: pointer;
                border-bottom: 2px solid white;
            }
        }
    }

    &:hover {
        cursor: pointer;
        border-bottom: '2px solid red';
    }
`;
const NavbarFixDropdown = styled.li`
    list-style: none;
    margin-right: 15px;
    position: none;
    border: 2px solid transparent;
    padding: 0 10px 4px 10px;
    ul {
        position: absolute;
        right: 0;
        background-color: white;
        width: 216px;
        margin-top: '20px';
        box-shadow: rgb(47 63 78 / 25%) 0px 0px 8px 0px;
        li {
            padding: 13px 15px;
            font-size: 1rem;
            &:hover {
                color: #0d6efd;
                cursor: pointer;
            }
        }
    }

    &:hover {
        cursor: pointer;
        border-bottom: '2px solid red';
    }
`;

const IconStyle = styled.div`
    display: inline-block;
    margin-right: 15px;
    padding-top: 5px;
`;

// 레이아웃 함수 정의
const Layout: React.FC<{}> = () => {
    const [dropState, setDropState] = useState({
        myInfoDropState: false,
        createDropState: false,
    });

    const dropStateHanlder = (DropStateIndex: string, state: boolean) => {
        switch (DropStateIndex) {
            case 'myInfoDropState':
                setDropState({ ...dropState, myInfoDropState: !state });
                break;
            case 'createDropState':
                setDropState({ ...dropState, createDropState: !state });
                break;
        }
    };

    return (
        <HeaderLayout>
            {/* 헤더부분 */}
            <Container>
                {/* 로고 및 마이페이지 */}
                <LogoContainer>
                    <LogoImgContainer>
                        <Link href="/">
                            <img src="https://nftmania.io/views/_layout/bootstrap/images/nftmania-logo-main.png" />
                        </Link>
                    </LogoImgContainer>
                    <MenuContainer
                        onMouseEnter={() =>
                            dropStateHanlder('myInfoDropState', false)
                        }
                        onMouseLeave={() =>
                            dropStateHanlder('myInfoDropState', true)
                        }
                    >
                        <IoPersonCircleOutline size="32" />
                        {dropState.myInfoDropState && (
                            <ul>
                                <li>
                                    <IconStyle>
                                        <BsPersonSquare size="16px" />
                                    </IconStyle>
                                    <Link href={{ pathname: '/account' }}>
                                        MyPage
                                    </Link>
                                </li>
                                <li>
                                    <IconStyle>
                                        <BsToggles size="16px" />
                                    </IconStyle>
                                    My Offers
                                </li>
                                <li>
                                    <IconStyle>
                                        <BsGear size="16px" />
                                    </IconStyle>
                                    <Link
                                        href={{ pathname: '/accountsettings' }}
                                    >
                                        Account Setttings
                                    </Link>
                                </li>
                                <li>
                                    <IconStyle>
                                        <BsBucket size="16px" />
                                    </IconStyle>
                                    My Tokens
                                </li>
                            </ul>
                        )}
                    </MenuContainer>
                </LogoContainer>
                {/* 검색창 및 nav */}
                <SearchNavbarContainer>
                    <SearchContainer>
                        <form>
                            <label>
                                <BiSearch size="20" />
                                <input
                                    type="text"
                                    placeholder="Search items, collections, and accounts"
                                />
                            </label>
                        </form>
                    </SearchContainer>

                    <NavbarContainer>
                        <li>
                            <Link href="/assetlist">Search</Link>
                        </li>
                        {/* <li>
                            <a
                                href="https://coinone.co.kr/exchange/trade/rush/krw"
                                target="_blank"
                            >
                                RUSH Purchase
                            </a>
                        </li>*/}
                        <li>
                            <Link href="/schedule">Auction Schedule</Link>
                        </li>
                        <li>
                            <Link href="/tradinghistory">Trading History</Link>
                        </li>
                        <li>
                            <Link href="/ranking">Rankings</Link>
                        </li>
                        {!dropState.createDropState && (
                            <NavbarFixDropdown
                                onMouseEnter={() =>
                                    dropStateHanlder('createDropState', false)
                                }
                                onMouseLeave={() =>
                                    dropStateHanlder('createDropState', true)
                                }
                            >
                                <Link href="/collection">Create</Link>
                            </NavbarFixDropdown>
                        )}
                        {dropState.createDropState && (
                            <NavbarDropdown
                                onMouseEnter={() =>
                                    dropStateHanlder('createDropState', false)
                                }
                                onMouseLeave={() =>
                                    dropStateHanlder('createDropState', true)
                                }
                            >
                                Create
                                <ul>
                                    <li>
                                        <IconStyle>
                                            <BiCollection />
                                        </IconStyle>
                                        <Link href="/collection">
                                            My Collections
                                        </Link>
                                    </li>
                                </ul>
                            </NavbarDropdown>
                        )}
                    </NavbarContainer>
                </SearchNavbarContainer>
            </Container>
        </HeaderLayout>
    );
};

export default Layout;
