import { AppProps } from 'next/app';
import { NextPage } from 'next';
import { wrapper } from '../store';

import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import '../components/styles/Swiper-styles.scss';
import GlobalStyle from '../components/styles/global-styles';

const MyApp: NextPage<AppProps> = ({ Component, pageProps }: AppProps) => {
    return (
        <>
            <GlobalStyle />
            <Component {...pageProps} />
        </>
    );
};

export default wrapper.withRedux(MyApp);
