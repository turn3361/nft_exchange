import { NextPage } from 'next';
import React from 'react';
import { AiFillStar } from 'react-icons/ai';
import { BiChevronDown, BiSearchAlt2, BiShuffle } from 'react-icons/bi';
import {
    BsFillCollectionFill,
    BsFillEnvelopeFill,
    BsFillTagFill,
} from 'react-icons/bs';
import styled from 'styled-components';
import Layout from '../components/layout/NavBar';
const BodyContainer = styled.div`
    display: flex;
    flex-direction: row;
`;

const BodyLeftContainer = styled.div`
    flex: 4.5;
    border-right: 1px solid rgb(229, 232, 235);
`;

const NavBarContainer = styled.div`
    border-bottom: 1px solid rgb(229, 232, 235);
`;

const NavBarUl = styled.ul`
    list-style: none;
    display: flex;
    align-items: center;
    height: 66px;
    padding: 12px 10px;
`;

const NavBarLi = styled.li`
    padding: 10px 20px;
    border-radius: 5px;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 50px;
    margin-left: 10px;
    color: rgb(138, 147, 155);
    img {
        width: 30px;
        height: 30px;
        margin-right: 8px;
    }
    &:hover {
        background-color: rgb(229, 232, 235);
        color: rgb(47, 63, 78);
        cursor: pointer;
    }
`;
const ListBodyContainer = styled.div`
    margin: auto 31px;
    border: 1px solid rgb(229, 232, 235);
    border-radius: 5px;
    overflow: auto;
    height: calc(100vh - 303px);
    margin-top: 30px;
`;
const ListBodyHeader = styled.div`
    padding: 20px 0 20px 20px;
    border-bottom: 1px solid rgb(229, 232, 235);
    font-size: 18px;
    font-weight: bold;
    display: flex;
    align-items: center;
    p {
        margin-left: 10px;
    }
`;
const ListBodyTableContainer = styled.div`
    height: 32px;
    background-color: rgb(251, 253, 255);
    width: 100%;
    overflow-x: auto;
    display: flex;
    flex-direction: row;
    position: sticky;
    border-bottom: 1px solid rgb(229, 232, 235);
`;
const ListBodyTableTitleCell = styled.div`
    padding: 8px 4px 4px 16px;
    flex-basis: 180px;
    align-items: center;
    overflow-x: auto;
    background-color: #fff;
`;
const ListBodyTableTitleCellImg = styled(ListBodyTableTitleCell)`
    flex-basis: 300px;
`;
const ListBodyItemContainer = styled.div`
    width: 100%;
    height: 80px;
    display: flex;
    align-items: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    overflow-y: auto;
`;
const ListBodyItem = styled(ListBodyTableTitleCell)`
    height: 70px;
    display: flex;
    align-items: center;
    div {
        display: flex;
        flex-direction: row;
        align-items: center;
        text-overflow: ellipsis;
    }
    img {
        width: 22px;
        height: 22px;
        border-radius: 50%;
        margin-right: 5px;
    }
    span {
        color: #039be5;
        font-size: 14px;
    }
    p {
        margin-left: 10px;
    }
`;
const ListBodyItemImg = styled(ListBodyTableTitleCellImg)`
    display: flex;
    align-items: center;
    height: 70px;
`;

const BodyRightListDropContainer = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 13px 12px;
    border-bottom: 1px solid rgba(229, 232, 235);
    div {
        display: flex;
        align-items: center;
    }
    p {
        font-weight: 600;
        font-size: 16px;
        margin-left: 10px;
    }
    &:hover {
        cursor: pointer;
    }
`;
const BodyRightListDropStatusUl = styled.ul`
    border-bottom: 1px solid rgba(229, 232, 235);
    display: flex;
    flex-flow: wrap;
    padding: 20px;

    li {
        display: flex;
        text-align: center;
        justify-content: center;
        align-items: center;
        width: calc(50% - 8px);
        height: 50px;
        border: 1px solid rgb(229, 232, 235);
        border-radius: 5px;
        margin: 4px;
        padding: 10px;
        &:hover {
            cursor: pointer;
            box-shadow: rgb(47 63 78 / 25%) 0px 0px 8px 0px;
            transition: all 0.2s ease 0s;
        }
    }
`;

const CollectionsFilterContainer = styled.div`
    padding: 20px;
    display: flex;
    flex-flow: wrap;
    border-bottom: 1px solid rgba(229, 232, 235);
`;

const CollectionsFilter = styled.div`
    margin-bottom: 12px;
    border: 1px solid rgb(229, 232, 235);
    height: 45px;
    padding: 0 10px;
    border-radius: 5px;
    display: flex;
    align-items: center;
    flex: 1 0 0%;
    font-size: 18px;
    input {
        padding: 1px 10px;
    }
`;
const CollectionsFilterList = styled.ul`
    width: 100%;
    max-height: 207px;
    padding: 4px 0;
    display: flex;
    flex-direction: column;

    li {
        display: flex;
        align-items: center;
        color: rgb(47, 63, 78);
        height: 40px;
        padding: 0 8px;
        &:hover {
            cursor: pointer;
        }
    }
    img {
        display: flex;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        border: 1px solid rgb(229, 232, 235);
        justify-content: center;
        transition: all 400ms ease 0s;
        object-fit: cover;

        &:hover {
            transform: scale(1.1);
        }
    }
`;
const Tradinghistory: NextPage = () => {
    return (
        <>
            <Layout />
            <BodyContainer>
                <BodyLeftContainer>
                    <NavBarContainer>
                        <NavBarUl>
                            <NavBarLi>All</NavBarLi>
                            <NavBarLi>
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/1.art.png" />
                                ART
                            </NavBarLi>
                            <NavBarLi>
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/2.Illust.png" />
                                ILLUST
                            </NavBarLi>
                            <NavBarLi>
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/3.entertain.png" />
                                ENTERTAINMENT
                            </NavBarLi>
                            <NavBarLi>
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/4.sports.png" />
                                SPORTS
                            </NavBarLi>
                            <NavBarLi>
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/5.domain.png" />
                                DOMAIN
                            </NavBarLi>
                            <NavBarLi>
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/6.universepng.png" />
                                UNIVERSE
                            </NavBarLi>
                            <NavBarLi>
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/7.freemarket.png" />
                                FREE MARKET
                            </NavBarLi>
                        </NavBarUl>
                    </NavBarContainer>

                    <ListBodyContainer>
                        <ListBodyHeader>
                            <BiShuffle size="25" />
                            <p>Trading History</p>
                        </ListBodyHeader>
                        <ListBodyTableContainer>
                            <ListBodyTableTitleCell>
                                Event
                            </ListBodyTableTitleCell>
                            <ListBodyTableTitleCellImg>
                                Item
                            </ListBodyTableTitleCellImg>
                            <ListBodyTableTitleCell>
                                Unit Price
                            </ListBodyTableTitleCell>
                            <ListBodyTableTitleCell>
                                Quantity
                            </ListBodyTableTitleCell>
                            <ListBodyTableTitleCell>
                                From
                            </ListBodyTableTitleCell>
                            <ListBodyTableTitleCell>To</ListBodyTableTitleCell>
                            <ListBodyTableTitleCell>
                                Date
                            </ListBodyTableTitleCell>
                        </ListBodyTableContainer>

                        <ListBodyItemContainer>
                            <ListBodyItem>
                                <BsFillEnvelopeFill size="20" />
                                <p>Offer</p>
                            </ListBodyItem>
                            <ListBodyItemImg>'Dave'</ListBodyItemImg>
                            <ListBodyItem>34,570</ListBodyItem>
                            <ListBodyItem>1</ListBodyItem>
                            <ListBodyItem>
                                <div>
                                    <img src="https://nftmania.io/views/_layout/bootstrap/images/user-sample-img.png" />
                                    <span>609C8261455A8</span>
                                </div>
                            </ListBodyItem>
                            <ListBodyItem>
                                <div>
                                    <img src="https://nftmania.io/uploads/account/account_608bbee0c9e7a.jpg" />
                                    <span>609C8261455A8</span>
                                </div>
                            </ListBodyItem>
                            <ListBodyItem>
                                <span>2 hours ago</span>
                            </ListBodyItem>
                        </ListBodyItemContainer>
                    </ListBodyContainer>
                </BodyLeftContainer>

                <div
                    style={{
                        flex: 1,
                        overflow: 'auto',
                        display: 'block',
                        height: 'calc(100vh - 174px)',
                    }}
                >
                    <div>
                        <BodyRightListDropContainer>
                            <div>
                                <AiFillStar size="20" />
                                <p>Event Type</p>
                            </div>
                            <BiChevronDown
                                size="40"
                                color="rgba(4, 17, 29, 0.5)"
                            />
                        </BodyRightListDropContainer>

                        <BodyRightListDropStatusUl>
                            <li>Listing</li>
                            <li>Sales</li>
                            <li>Bids</li>
                            <li>Transfer</li>
                        </BodyRightListDropStatusUl>
                    </div>
                    <div>
                        <BodyRightListDropContainer>
                            <div>
                                <BsFillCollectionFill size="20" />
                                <p>Collections</p>
                            </div>
                            <BiChevronDown
                                size="40"
                                color="rgba(4, 17, 29, 0.5)"
                            />
                        </BodyRightListDropContainer>
                        <CollectionsFilterContainer>
                            <CollectionsFilter>
                                <BiSearchAlt2 size="30" />
                                <input placeholder="Filter" />
                            </CollectionsFilter>

                            <CollectionsFilterList>
                                <li>
                                    <img src="https://nftmania.io/uploads/collection/collection_608a9aaa57b98.png" />
                                    <label>
                                        <input type="checkbox" />
                                        SpecialForceNFT
                                    </label>
                                </li>
                            </CollectionsFilterList>
                        </CollectionsFilterContainer>
                    </div>
                </div>
            </BodyContainer>
        </>
    );
};

export default Tradinghistory;
