import { NextPage } from 'next';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import {
    AiOutlineEye,
    AiOutlineShareAlt,
    AiOutlineUnorderedList,
} from 'react-icons/ai';
import {
    BsArrowRepeat,
    BsArrowUpDown,
    BsCardChecklist,
    BsFillPersonLinesFill,
    BsGraphUp,
    BsThreeDotsVertical,
} from 'react-icons/bs';
import { MdKeyboardArrowDown } from 'react-icons/md';
import { IoIosArrowDown } from 'react-icons/io';
import styled from 'styled-components';
import Layout from '../../../components/layout/NavBar';

const BodyContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

const TitleContainer = styled.div`
    position: sticky;
    border-bottom: 1px solid rgb(229, 232, 235);
    top: 173px;
    height: 70px;
    width: 100%;
    padding: 10px 20px;
    z-index: 2;
    right: 0;
    background-color: rgb(251, 253, 255);
`;
const Title = styled.div`
    display: flex;
    justify-content: flex-end;
    margin: auto;
    padding: 0 20px;
    align-items: center;
    max-width: 1280px;
    height: 100%;
`;

const AssetContainer = styled.div`
    width: 1280px;
    max-width: 100%;
    padding: 8px 0px 16px;
    display: flex;
    flex-direction: row;
`;
const OwnerSummary = styled.div`
    flex: 3 0 0%;
    max-width: 43%;
    width: 0;
`;
const AssetImgWrap = styled.div`
    margin: 20px;
    border: 1px solid rgb(229, 232, 235);
    border-radius: 5px;
    overflow: hidden;

    div {
        cursor: pointer;
        width: 100%;
        background-color: #fff;
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;
        min-height: 430px;
    }
    img {
        object-fit: contain;
        width: 100%;
        vertical-align: middle;
    }
`;

const AssetImgDetail = styled.div`
    margin: 20px;
    display: grid;
    grid-template-rows: repeat(2, 100px);
    grid-template-columns: repeat(5, 1fr);
    row-gap: 10px;
    column-gap: 10px;
    div {
        border: 1px solid rgb(229, 232, 235);
        border-radius: 5px;
    }
    img {
        width: 100%;
        height: 100%;
        object-fit: contain;
        cursor: pointer;
    }
`;

const AssetMainInfo = styled.div`
    flex: 4 0 0%;
    margin-left: -20px;
    display: flex;
    flex-direction: column;
`;
const AssetBasic = styled.div`
    margin: 20px;
    display: flex;
    justify-content: flex-start;
    flex-flow: column wrap;
`;
const AssetCollection = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 12px;
`;
const CollectionName = styled.div`
    display: flex;
    align-items: center;
    width: 420px;
    max-width: 100%;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;

    a {
        color: rgb(32, 129, 226);
        font-size: 16px;
    }
`;
const CollectionLinks = styled.div`
    position: relative;
    display: inline-flex;
    vertical-align: middle;
    button {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0px;
        position: relative;
        flex: 1 1 auto;
        cursor: pointer;
        background-color: #f8f9fa;
        border-color: #f8f9fa;
        padding: 0.375rem 0.75rem;
        color: #000;
        vertical-align: middle;
        transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out,
            border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        line-height: 1.5;
        font-weight: 400;
        text-align: center;
        text-decoration: none;
        vertical-align: middle;
        user-select: none;
    }
`;

const CollectionLinksBtnGroup = styled.div`
    margin-left: -1px;
    position: relative;
    display: inline-flex;
    vertical-align: middle;
    button {
        position: relative;
        flex: 1 1 auto;
    }
`;
const CollectionLinksDropdown = styled.ul`
    position: absolute;
    top: 100%;
    min-width: 10rem;
    padding: 0.5rem 0;
    margin: 0;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 0.25rem;
    li {
        display: list-item;
        text-align: -webkit-match-paren;
        margin: 5px;
        &:hover {
        }
    }
    a {
        display: block;
        width: 100%;
        padding: 0.25rem 1rem;
        font-weight: 400;
        color: #212529;
        text-align: inherit;
        text-decoration: none;
        white-space: nowrap;
        background-color: transparent;
        border: 0;
    }
`;

const AssetName = styled.div`
    width: 100%;
    font-size: 30px;
    font-weight: 600;
    overflow: hidden;
    text-overflow: ellipsis;
    img {
        height: 25px;
        line-height: 45px;
        margin-right: 5px;
        vertical-align: baseline;
        margin-right: 10px;
    }
`;
const AssetCreator = styled.div`
    margin: 20px 20px 20px 0;
    font-size: 14px;
    display: flex;
    align-items: center;
    a {
        display: flex;
        align-items: center;
        overflow: hidden;
        text-overflow: ellipsis;
        color: rgb(155, 155, 155);
        text-decoration: none;
    }
    img {
        width: 22px;
        height: 22px;
        border-radius: 50%;
        margin-right: 5px;
        vertical-align: middle;
    }
    span {
        color: #039be5;
        padding-left: 5px;
    }
`;
const AssetHit = styled.div`
    display: flex;
    justify-content: center;
    margin-left: 10px;
    svg {
        margin: 0px 5px;
    }
`;

const AuctionDeadLine = styled.div`
    margin: 0 20px;
    color: #084298;
    background-color: #cfe2ff;
    border-color: #b6d4fe;
    position: relative;
    padding: 1rem 1rem;
    border: 1px solid transparent;
    border-radius: 0.25rem;
`;

const AssetSummary = styled.div`
    margin: 20px;
    width: 100%;
    display: flex;
    flex-direction: column;

    text-align: left;
    border: 1px solid rgba(0, 0, 0, 0.125);
    border-radius: 0;
    overflow-anchor: none;

    h2 {
        margin-bottom: 0;
    }
    button {
        display: flex;
        border-top-left-radius: 0.25rem;
        border-top-right-radius: 0.25rem;
        background: #fff;
        color: #000;
        font-size: 17px;
        justify-content: center;
    }
    svg {
        margin-right: 15px;
    }
    img {
        width: 22px;
        height: 22px;
        border-radius: 50%;
        margin-right: 5px;
        vertical-align: middle;
    }
`;
const AccordionCollapseTop = styled.div`
    border-bottom: 1px solid rgba(0, 0, 0, 0.125);
    padding: 1rem 1.25rem;
`;
const AccordionCollapseBody = styled.div`
    max-height: 200px;
    overflow: auto;
    padding: 1rem 1.25rem;
    font-size: 14px;

    a {
        color: rgb(155, 155, 155);
    }
    span {
        color: #039be5;
    }
    div {
        margin-top: 40px;
    }
`;
const AccordionCollapseBodyWarning = styled.div`
    width: 100%;
    color: #664d03;
    background-color: #fff3cd;
    border-color: #ffecb5;
    margin: 0.25rem 20px 20px 20px;
    padding: 1rem 1rem;
    border-radius: 0.25rem;
`;

const AccordionCollapseFooter = styled.div`
    width: 100%;
    margin: 40px 20px 20px 20px;
    padding: 02px 15px;
    border: 1px solid rgb(229, 232, 235);
    border-radius: 5px;
    overflow: hidden;
    background: rgb(251, 253, 255);
    h2 {
        font-size: 30px;
        font-weight: 600;
        margin: 50px 0 20px 0;
    }
    span {
        margin-right: 10px;
    }
    p {
        color: rgb(138, 147, 155);
        font-size: 15px;
        margin-left: 10px;
    }
`;

const AccordionCollapsebottomMidText = styled.div`
    margin-top: 20px;
    margin-bottom: 40px;
`;

const AboutContainer = styled.div`
    width: 1280px;
    max-width: 100%;
    border: 1px solid rgb(229, 232, 235);

    img {
        width: 80px;
        height: 80px;
        margin: 3px 10px 0 0;
    }
`;

const AboutTitle = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 16px 20px;
    border-bottom: 1px solid rgb(229, 232, 235);
    div {
        display: flex;
        align-items: center;
        justify-content: center;
    }
    span {
        margin-left: 10px;
    }
`;

const AboutItems = styled.div`
    display: flex;
    flex-direction: row;
    margin: 20px;
    justify-content: start;
`;

const PriceHistoryContainer = styled.div`
    width: 1280px;
    max-width: 100%;
    border: 1px solid rgb(229, 232, 235);
    margin-top: 40px;
`;
const TragingHistoryContainer = styled.div`
    width: 1280px;
    max-width: 100%;
    border: 1px solid rgb(229, 232, 235);
    margin: 40px 0px;
`;
const PriceHistoryBodyTop = styled.div`
    border-bottom: 1px solid rgb(229, 232, 235);
`;
const PriceHistoryBodyBottom = styled.div``;

const PriceHistoryBody = styled.div`
    margin: 30px;

    select {
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        width: auto;
        padding: 0.375rem 2.25rem 0.375rem 0.75rem;
    }
    div {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
    }
`;
const OffersNavbar = styled.div`
    border-bottom: 1px solid rgb(229, 232, 235);
    max-height: 332px;
    width: 100%;
    div {
        margin-left: 4px;
        display: flex;
        flex-direction: row;
    }
    p {
        padding: 4px;
        flex: 1 0 140px;
    }
`;
const OffersDataUpdateButtonContainer = styled.div`
    border-bottom: 1px solid rgb(229, 232, 235);
    max-height: 332px;
    width: 100%;

    button {
        width: 95%;
        height: 60px;
        border: 1px solid #e8632e;
        color: #e8632e;
        border-radius: 0.3rem;
        padding: 0.5rem 1rem;
        font-size: 1.25rem;
        margin: 30px;
        &:hover {
            background-color: #e8632e;
            border: 1px solid #e8632e;
            color: white;
            cursor: pointer;
        }
    }
`;

const TradingHistoryCheckContainer = styled.div`
    border-bottom: 1px solid rgb(229, 232, 235);
    max-height: 332px;
    width: 100%;
    div {
        margin-left: 4px;
        display: flex;
        flex-direction: row;
        margin: 10px 0px;
    }
    span {
        padding: 8px;
        font-size: 16px;
        display: flex;
        align-items: center;
        input {
            width: 18px;
            height: 18px;
        }
    }
`;
const Index: NextPage = () => {
    const router = useRouter();
    const [modalState, setModalState] = useState<{
        shareModal: boolean;
        reportModal: boolean;
    }>({
        shareModal: false,
        reportModal: false,
    });

    return (
        <>
            <Layout />
            <BodyContainer>
                <TitleContainer>
                    <Title>jejuislandbreak</Title>
                </TitleContainer>

                <AssetContainer>
                    <OwnerSummary>
                        <AssetImgWrap>
                            <div>
                                <img src="https://nftmania.io/uploads/product/product_60ab25f01ac4a.jpg" />
                            </div>
                        </AssetImgWrap>
                        <AssetImgDetail>
                            <div>
                                <img src="https://nftmania.io/uploads/product/product_60ab25f01ac4a.jpg" />
                            </div>
                        </AssetImgDetail>
                    </OwnerSummary>
                    <AssetMainInfo>
                        <AssetBasic>
                            <AssetCollection>
                                <CollectionName>
                                    <a href="">jejuislandbreak</a>
                                </CollectionName>
                                <CollectionLinks>
                                    <button>
                                        <BsArrowRepeat size="18" />
                                    </button>
                                    <CollectionLinksBtnGroup>
                                        <button>
                                            <AiOutlineShareAlt size="18" />
                                            <MdKeyboardArrowDown size="18" />
                                        </button>
                                        {modalState.shareModal && (
                                            <CollectionLinksDropdown>
                                                <li>
                                                    <a href="/">Copy Link</a>
                                                </li>
                                                <li>
                                                    <a href="/">
                                                        Share on Facebook
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/">
                                                        Share on Twitter
                                                    </a>
                                                </li>
                                            </CollectionLinksDropdown>
                                        )}
                                    </CollectionLinksBtnGroup>
                                    <CollectionLinksBtnGroup>
                                        <button>
                                            <BsThreeDotsVertical size="18" />
                                            <MdKeyboardArrowDown size="18" />
                                        </button>
                                        {modalState.reportModal && (
                                            <CollectionLinksDropdown>
                                                <li>
                                                    <a href="/">Report</a>
                                                </li>
                                            </CollectionLinksDropdown>
                                        )}
                                    </CollectionLinksBtnGroup>
                                </CollectionLinks>
                            </AssetCollection>

                            <AssetName>
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/chainDecentral.png" />
                                jejuislandbreak
                            </AssetName>
                            <AssetCreator>
                                <a href="/">
                                    <img src="https://nftmania.io/views/_layout/bootstrap/images/user-sample-img.png" />
                                    Owned by
                                    <span>60AB24629F556</span>
                                </a>
                                <AssetHit>
                                    <AiOutlineEye size="18" />
                                    31 view
                                </AssetHit>
                            </AssetCreator>
                        </AssetBasic>
                        <AuctionDeadLine>Transaction type :</AuctionDeadLine>
                        <AssetSummary>
                            <AccordionCollapseTop>
                                <h2>
                                    <button>
                                        <BsFillPersonLinesFill size="20" />
                                        Details
                                    </button>
                                </h2>
                            </AccordionCollapseTop>
                            <AccordionCollapseBody>
                                <a href="/">
                                    <img src="https://nftmania.io/views/_layout/bootstrap/images/user-sample-img.png" />
                                    Created by
                                    <span> 60AB24629F556</span>
                                </a>
                                <div>jejuislandbreak</div>
                            </AccordionCollapseBody>
                        </AssetSummary>
                        <AccordionCollapseBodyWarning>
                            3% royalty will be provided to copyright holders
                            when selling.
                            <br />
                            <br />
                            1% royalty will be provided to NFT token issuers for
                            sale.
                        </AccordionCollapseBodyWarning>
                        <AccordionCollapseFooter>
                            <h2>
                                <span>Ξ</span>
                                Nondisclosure
                            </h2>
                            <div>
                                <p>
                                    Transfer of Ownership Gas Cost : 0.00556
                                    (ETH)
                                </p>

                                <p>Total Cost : 0.00556 (ETH)</p>
                            </div>
                            <AccordionCollapsebottomMidText>
                                <p>
                                    수수료는 이더리움 금액 기준으로 환산되어
                                    적용 됩니다.
                                </p>
                                <p>
                                    Fees are converted and applied on an
                                    Ethernet Rium monetary basis.
                                </p>
                            </AccordionCollapsebottomMidText>
                        </AccordionCollapseFooter>
                    </AssetMainInfo>
                </AssetContainer>

                <div>
                    <AboutContainer>
                        <AboutTitle>
                            <div>
                                <BsCardChecklist size="25" />
                                <span>About jejuislandbreak </span>
                            </div>
                            <IoIosArrowDown />
                        </AboutTitle>
                        <AboutItems>
                            <img src="https://nftmania.io/uploads/collection/collection_60ab2555c6728.jpg" />
                            <p>awake</p>
                        </AboutItems>
                    </AboutContainer>
                    <PriceHistoryContainer>
                        <PriceHistoryBodyTop>
                            <AboutTitle>
                                <div>
                                    <BsGraphUp size="25" />
                                    <span>Price History</span>
                                </div>
                                <IoIosArrowDown />
                            </AboutTitle>
                            <PriceHistoryBody>
                                <select>
                                    <option value="-1">All Time</option>
                                    <option value="7">Last 7 Days</option>
                                    <option value="30">Last 10 Days</option>
                                </select>
                                <div>
                                    <img src="https://nftmania.io/views/_layout/bootstrap/images/no-chart-data.svg" />
                                    <p>No trading data yet</p>
                                </div>
                            </PriceHistoryBody>
                        </PriceHistoryBodyTop>
                        <PriceHistoryBodyBottom>
                            <AboutTitle>
                                <div>
                                    <AiOutlineUnorderedList size="20" />
                                    <span>offers</span>
                                </div>
                                <IoIosArrowDown />
                            </AboutTitle>
                            <OffersNavbar>
                                <div>
                                    <p>From</p>
                                    <p>Price</p>
                                    <p>Expriation</p>
                                </div>
                            </OffersNavbar>
                            <OffersDataUpdateButtonContainer>
                                <button>Update Data</button>
                            </OffersDataUpdateButtonContainer>
                        </PriceHistoryBodyBottom>
                    </PriceHistoryContainer>
                    <TragingHistoryContainer>
                        <PriceHistoryBodyBottom>
                            <AboutTitle>
                                <div>
                                    <BsArrowUpDown size="20" />
                                    <span>Trading History</span>
                                </div>
                                <IoIosArrowDown />
                            </AboutTitle>
                            <TradingHistoryCheckContainer>
                                <div>
                                    <span>
                                        <input type="checkbox" />
                                        <label>Listing</label>
                                    </span>
                                    <span>
                                        <input type="checkbox" />
                                        <label>Sales</label>
                                    </span>
                                    <span>
                                        <input type="checkbox" />
                                        <label>Bids</label>
                                    </span>
                                    <span>
                                        <input type="checkbox" />
                                        <label>Transfers</label>
                                    </span>
                                </div>
                            </TradingHistoryCheckContainer>
                            <OffersDataUpdateButtonContainer>
                                <button>Update Data</button>
                            </OffersDataUpdateButtonContainer>
                        </PriceHistoryBodyBottom>
                    </TragingHistoryContainer>
                </div>
            </BodyContainer>
        </>
    );
};

export default Index;
