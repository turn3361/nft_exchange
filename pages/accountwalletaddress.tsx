import { NextPage } from 'next';
import React from 'react';
import { BiWallet } from 'react-icons/bi';
import { BsBucket, BsGearFill } from 'react-icons/bs';
import styled from 'styled-components';
import Footer from '../components/layout/Footer';
import Layout from '../components/layout/NavBar';

const BodyContainer = styled.div`
    display: flex;
    height: 100%;
    margin-bottom: -50px;
`;
const BodyMenuContainer = styled.div`
    align-self: flex-start;
    border-right: 1px solid rgb(229, 232, 235);
    flex-shrink: 0;
    height: calc(100vh - 153px);
    position: sticky;
    top: 64px;
    width: 380px;
    color: rgba(47, 63, 78, 0.75);
`;
const BodyMenuMywallet = styled.div`
    border-bottom: 1px solid rgb(229, 232, 235);
    color: rgb(4, 17, 29);
`;
const BodyMenuMywalletIconContainer = styled.div`
    color: rgba(47, 63, 78, 0.75);
    -webkit-box-align: center;
    align-items: center;
    display: flex;
    font-size: 16px;
    font-weight: 600;
    padding: 20px 12px;
    user-select: none;
    svg {
        margin-right: 10px;
    }
`;
const BodyMenuMywalletUserInfo = styled.div`
    border-top: 1px solid rgb(229, 232, 235);

    background: rgb(237, 251, 255);
    color: rgb(4, 17, 29);

    a {
        color: rgba(47, 63, 78, 0.75);
        padding: 20px;
        display: flex;
        align-items: center;
        -webkit-box-align: center;
        width: 100%;
        text-decoration: none;
        img {
            object-fit: cover;
            width: 32px;
            height: 32px;
            border-radius: 50%;
            margin-right: 10px;
            vertical-align: middle;
        }
    }
`;
const BodyMenuGeneralContainer = styled.div`
    color: rgb(47, 63, 78);
    background: rgb(251, 253, 255);
    pointer-events: none;
    cursor: pointer;
    font-size: 16px;
    font-weight: 600;
    padding: 10px 12px;
    display: flex;
    align-items: center;
    -webkit-box-align: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    a {
        color: rgb(4, 17, 29);
        width: 100%;
        padding: 10px 0;
        text-decoration: none;
        svg {
            margin-right: 10px;
            vertical-align: middle;
        }
    }
`;
const BodyMenuMyTokenContainer = styled.div`
    cursor: pointer;
    font-size: 16px;
    font-weight: 600;
    padding: 10px 12px;
    display: flex;
    align-items: center;
    -webkit-box-align: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    a {
        color: rgba(47, 63, 78, 0.75);
        width: 100%;
        padding: 10px 0;
        text-decoration: none;
        svg {
            margin-right: 10px;
            vertical-align: middle;
        }
    }
`;
const BodyFormContainer = styled.form`
    display: flex;
    width: 100%;
    max-width: 800px;
    margin: 0;
    padding: 0;
`;
const BodyItemContainer = styled.div`
    max-width: 800px;
    margin: 0px 8vw;
    flex: 1 0 0%;
    h2 {
        margin: 60px 0 20px;
        font-size: 24px;
        font-weight: 600;
        line-height: 1.1;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    label {
        margin-bottom: 0.5rem;
    }
    div {
        margin-bottom: 1.5rem;
        position: relative;
        display: flex;
        flex-wrap: wrap;
        align-items: stretch;
        width: 100%;
        input {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            min-width: 400px;
            position: relative;
            flex: 1 1 auto;
            width: 1%;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            border-radius: 0.25rem;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            background-color: #e9ecef;
            opacity: 1;
        }
        button {
            margin-left: -1px;
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            border: 1px solid #e8632e;
            color: #e8632e;
            position: relative;
            z-index: 2;
            font-weight: 400;
            line-height: 1.5;
            text-align: center;
            text-decoration: none;
            vertical-align: middle;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            border-radius: 0.25rem;
        }
    }
    a {
        border: 1px solid #e8632e;
        color: #e8632e;
        margin-top: 1.5rem;
        padding: 0.5rem 1rem;
        font-size: 1.25rem;
        border-radius: 0.3rem;
        display: inline-block;
        font-weight: 400;
        line-height: 1.5;
        text-align: center;
        text-decoration: none;
        vertical-align: middle;
        cursor: pointer;
        user-select: none;
        background-color: transparent;
        cursor: pointer;
        &:hover {
            background-color: #e8632e;
            color: #fff;
        }
    }
`;
const Accountwalletaddress: NextPage = () => {
    return (
        <>
            <Layout />
            <BodyContainer>
                <BodyMenuContainer>
                    <BodyMenuMywallet>
                        <BodyMenuMywalletIconContainer>
                            <BiWallet size="24" />
                            My Wallet
                        </BodyMenuMywalletIconContainer>
                        <BodyMenuMywalletUserInfo>
                            <a href="/accountwalletaddress">
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/user-sample-img.png" />
                                0x7a92.....2a5c28
                            </a>
                        </BodyMenuMywalletUserInfo>
                    </BodyMenuMywallet>
                    <BodyMenuGeneralContainer>
                        <a href="/accountsettings">
                            <BsGearFill size="24" />
                            General
                        </a>
                    </BodyMenuGeneralContainer>
                    <BodyMenuMyTokenContainer>
                        <a href="/accountmypoint">
                            <BsBucket size="24" />
                            My Tokens
                        </a>
                    </BodyMenuMyTokenContainer>
                </BodyMenuContainer>
                <BodyFormContainer>
                    <BodyItemContainer>
                        <h2>Settings</h2>
                        <label>Your Wallet Address</label>
                        <br />
                        <br />
                        <div>
                            <input defaultValue="0x7a92d4b067aacd8f85d00a7776290a343a2a5c28" />
                            <button>Copy</button>
                        </div>
                        <a href="/login_social/logout/redirect">Log Out</a>
                    </BodyItemContainer>
                </BodyFormContainer>
            </BodyContainer>
            <Footer />
        </>
    );
};

export default Accountwalletaddress;
