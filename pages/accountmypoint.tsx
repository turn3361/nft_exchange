import { NextPage } from 'next';
import React from 'react';
import { BiCalendarAlt, BiWallet } from 'react-icons/bi';
import { BsBucket, BsGearFill } from 'react-icons/bs';
import styled from 'styled-components';
import Footer from '../components/layout/Footer';
import Layout from '../components/layout/NavBar';

const BodyContainer = styled.div`
    display: flex;
    margin-bottom: -50px;
    min-height: calc(100vh - 293px);
`;
const BodyMenuContainer = styled.div`
    align-self: flex-start;
    border-right: 1px solid rgb(229, 232, 235);
    flex-shrink: 0;
    height: calc(100vh - 153px);
    position: sticky;
    top: 64px;
    width: 380px;
    color: rgba(47, 63, 78, 0.75);
`;
const BodyMenuMywallet = styled.div`
    border-bottom: 1px solid rgb(229, 232, 235);
    color: rgb(4, 17, 29);
`;
const BodyMenuMywalletIconContainer = styled.div`
    color: rgba(47, 63, 78, 0.75);
    -webkit-box-align: center;
    align-items: center;
    display: flex;
    font-size: 16px;
    font-weight: 600;
    padding: 20px 12px;
    user-select: none;
    svg {
        margin-right: 10px;
    }
`;
const BodyMenuMywalletUserInfo = styled.div`
    border-top: 1px solid rgb(229, 232, 235);
    color: rgb(47, 63, 78);
    background: rgb(251, 253, 255);

    a {
        color: rgba(47, 63, 78, 0.75);
        padding: 20px;
        display: flex;
        align-items: center;
        -webkit-box-align: center;
        width: 100%;
        text-decoration: none;
        img {
            object-fit: cover;
            width: 32px;
            height: 32px;
            border-radius: 50%;
            margin-right: 10px;
            vertical-align: middle;
        }
    }
`;
const BodyMenuGeneralContainer = styled.div`
    color: rgb(47, 63, 78);
    background: rgb(251, 253, 255);
    pointer-events: none;
    cursor: pointer;
    font-size: 16px;
    font-weight: 600;
    padding: 10px 12px;
    display: flex;
    align-items: center;
    -webkit-box-align: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    a {
        color: rgb(4, 17, 29);
        width: 100%;
        padding: 10px 0;
        text-decoration: none;
        svg {
            margin-right: 10px;
            vertical-align: middle;
        }
    }
`;
const BodyMenuMyTokenContainer = styled.div`
    background: rgb(237, 251, 255);
    color: rgb(4, 17, 29);
    cursor: pointer;
    font-size: 16px;
    font-weight: 600;
    padding: 10px 12px;
    display: flex;
    align-items: center;
    -webkit-box-align: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    a {
        color: rgba(47, 63, 78, 0.75);
        width: 100%;
        padding: 10px 0;
        text-decoration: none;
        svg {
            margin-right: 10px;
            vertical-align: middle;
        }
    }
`;
const BodyFormContainer = styled.form`
    max-width: 800px;
    margin: 0px 8vw;
    flex: 1 0 0%;
    ul {
        margin-top: 40px;
        border-bottom: 1px solid #dee2e6;
        display: flex;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;

        li {
            list-style: none;
            a {
                text-decoration: none;
                color: #000;
                button {
                    color: #495057;
                    background-color: #fff;
                    border-color: #dee2e6 #dee2e6 #fff;
                    margin-bottom: -1px;
                    background: 0 0;
                    border: 1px solid transparent;
                    border-top-left-radius: 0.25rem;
                    border-top-right-radius: 0.25rem;
                    cursor: pointer;
                    display: block;
                    padding: 0.5rem 1rem;
                    text-decoration: none;
                    transition: color 0.15s ease-in-out;
                    background-color: 0.15s ease-in-out;
                    border-color: 0.15s ease-in-out;
                    font-size: 1rem;
                    font-weight: 400;
                    line-height: 1.5;
                }
            }
        }
    }
`;
const BodyItemTitleContainer = styled.div`
    margin: 60px 0 20px;
    font-size: 24px;
    font-weight: 600;
    line-height: 1.1;
    display: flex;
    justify-content: space-between;
    align-items: center;
    a {
        background-color: #e8632e;
        border: 1px solid #e8632e;
        color: #fff;
        display: inline-block;
        font-weight: 400;
        line-height: 1.5;
        text-align: center;
        text-decoration: none;
        vertical-align: middle;
        cursor: pointer;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        border-radius: 0.25rem;
    }
`;
const BodyItemMyTokenContainer = styled.div`
    margin-bottom: 1.5rem;
    label {
        margin-bottom: 0.5rem;
    }
`;
const BodyItemMyTokenAvailableContainer = styled.div`
    margin-bottom: 1rem;
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: stretch;
    width: 100%;
    input {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        position: relative;
        flex: 1 1 auto;
        width: 1%;
        min-width: 0;
        background-color: #e9ecef;
        opacity: 1;
        display: block;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        margin: 0;
        font-family: inherit;
    }
    span {
        margin-left: -1px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        width: 70px;
        text-align: center;
        display: flex;
        align-items: center;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        white-space: nowrap;
        background-color: #e9ecef;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
    }
`;
const BodyItemMyTokenHistoryContainer = styled.div`
    padding: 0px;
`;
const BodyItemMyTokenHistoryListContainer = styled.div`
    border-radius: 5px;
    overflow: hidden;
    margin-bottom: 25px;
`;
const BodyItemMyTokenHistoryListHeader = styled.div`
    padding: 20px 0 20px 20px;
    border-bottom: 1px solid rgb(229, 232, 235);
    font-size: 18px;
    font-weight: bold;
    svg {
        margin-right: 10px;
    }
`;
const BodyItemMyTokenHistoryListTableContainer = styled.div`
    max-height: none;
    width: 100%;
    overflow-x: auto;
    display: flex;
    flex-direction: column;
    overflow-y: auto;
`;
const BodyItemMyTokenHistoryListTableHeader = styled.div`
    position: sticky;
    top: 0;
    z-index: 1;
    display: flex;
    div {
        padding-left: 16px;
        background-color: #fff;
        color: rgb(14, 14, 14);
        padding: 4px;
        align-items: center;
        border-bottom: 1px solid rgb(229, 232, 235);
        display: flex;
        flex: 1 0 140px;
        overflow-x: auto;
    }
`;
const Accountmypoint: NextPage = () => {
    return (
        <>
            <Layout />
            <BodyContainer>
                <BodyMenuContainer>
                    <BodyMenuMywallet>
                        <BodyMenuMywalletIconContainer>
                            <BiWallet size="24" />
                            My Wallet
                        </BodyMenuMywalletIconContainer>
                        <BodyMenuMywalletUserInfo>
                            <a href="/accountwalletaddress">
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/user-sample-img.png" />
                                0x7a92.....2a5c28
                            </a>
                        </BodyMenuMywalletUserInfo>
                    </BodyMenuMywallet>
                    <BodyMenuGeneralContainer>
                        <a href="/accountsettings">
                            <BsGearFill size="24" />
                            General
                        </a>
                    </BodyMenuGeneralContainer>
                    <BodyMenuMyTokenContainer>
                        <a href="/accountmypoint">
                            <BsBucket size="24" />
                            My Tokens
                        </a>
                    </BodyMenuMyTokenContainer>
                </BodyMenuContainer>
                <BodyFormContainer>
                    <ul>
                        <li>
                            <a href="/accountmypoint">
                                <button>ALL</button>
                            </a>
                        </li>
                        <li>
                            <a href="/accountmypoint/ETH">
                                <button>ETH</button>
                            </a>
                        </li>
                    </ul>
                    <div>
                        <BodyItemTitleContainer>
                            My Tokens
                            <a href="#">Send to NFTMANIA Wallet</a>
                        </BodyItemTitleContainer>
                        <BodyItemMyTokenContainer>
                            <label>Available Tokens</label>
                            <br />
                            <br />
                            <BodyItemMyTokenAvailableContainer>
                                <input defaultValue="0" />
                                <span>ETH</span>
                            </BodyItemMyTokenAvailableContainer>
                            <BodyItemMyTokenHistoryContainer>
                                <BodyItemMyTokenHistoryListContainer>
                                    <BodyItemMyTokenHistoryListHeader>
                                        <BiCalendarAlt size="25" />
                                        Token History
                                    </BodyItemMyTokenHistoryListHeader>
                                    <BodyItemMyTokenHistoryListTableContainer>
                                        <BodyItemMyTokenHistoryListTableHeader>
                                            <div>Date</div>
                                            <div>Symbol</div>
                                            <div>Purpose</div>
                                            <div>Tokens</div>
                                            <div>Tokens Remaining</div>
                                        </BodyItemMyTokenHistoryListTableHeader>
                                    </BodyItemMyTokenHistoryListTableContainer>
                                </BodyItemMyTokenHistoryListContainer>
                            </BodyItemMyTokenHistoryContainer>
                        </BodyItemMyTokenContainer>
                    </div>
                </BodyFormContainer>
            </BodyContainer>
            <Footer />
        </>
    );
};

export default Accountmypoint;
