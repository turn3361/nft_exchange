import { NextPage } from 'next';
import React from 'react';
import { AiOutlineInstagram } from 'react-icons/ai';
import { BiAt } from 'react-icons/bi';
import { BsChevronLeft, BsFillCollectionFill } from 'react-icons/bs';
import { FaTelegram } from 'react-icons/fa';
import { IoLogoDiscord, IoLogoFacebook, IoLogoTwitter } from 'react-icons/io5';
import styled from 'styled-components';
import Layout from '../../../components/layout/NavBar';

const BodyContainer = styled.div`
    display: flex;
    flex-direction: row;
`;

const CollectionMenu = styled.div`
    padding: 32px 16px;
    display: flex;
    position: fixed;
    width: 300px;
    height: 100%;
    border-right: 1px solid rgb(229, 232, 235);

    a {
        width: 270px;
        padding: 12px 16px;
        display: flex;
        position: fixed;
        background-color: rgb(251, 253, 255);
        align-items: center;
        color: rgba(53, 56, 64, 0.75);
        svg {
            font-size: 24px;
            margin-right: 10px;
            font-weight: 500;
        }
        &:hover {
            background-color: rgb(229, 232, 235);
            color: rgb(47, 63, 78);
            cursor: pointer;
        }
    }
`;

const CollectionEditContainer = styled.div`
    margin-left: 300px;
    vertical-align: top;
`;
const CollectionEditTop = styled.div`
    display: flex;
    align-items: center;
    background: rgb(251, 253, 255);
    border-bottom: 1px solid rgb(229, 232, 235);
    height: 72px;
    padding: 0 8px;
    position: fixed;
    width: 100%;
    z-index: 4;
    a {
        color: rgb(136, 136, 136);
        font-size: 14px;
        display: flex;
        align-items: center;
    }
    svg {
        margin-right: 10px;
    }
`;

const CollectionEditBodyContainer = styled.div`
    padding: 153px 30px 50px;
    display: flex;
    flex-direction: column;
    h1 {
        font-size: 26px;
        font-weight: 500;
        margin-top: 16px;
        margin-right: 32px;
        margin-bottom: 6px;
    }
`;
const CollectionBodyItemContainer = styled.div`
    margin-top: 28px;
    h5 {
        font-size: 18px;
        font-weight: 500;
        margin-bottom: 12px;
    }
    h6 {
        margin: 12px 0;
        font-size: 14px;
        color: rgb(138, 147, 155);
    }
`;
const CollectionBodyItemImgContainer = styled.div`
    border: 3px dashed rgb(204, 204, 204);
    border-radius: 5px;
    width: 160px;
    height: 160px;
    cursor: pointer;
    padding: 4px;
    margin: 6px 0 20px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    img {
        object-fit: contain;
        height: 100%;
        width: 100%;
        transition: opacity 400ms ease 0s;
        vertical-align: middle;
    }
    div {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        input {
            position: absolute;
            width: 100%;
            height: 100%;
            opacity: 0;
            cursor: pointer;
        }
    }
`;

const CollectionFeaturedImageContainer = styled(CollectionBodyItemImgContainer)`
    width: 300px;
    height: 200px;
`;

const CollecionNameContainer = styled(CollectionBodyItemContainer)`
    div {
        margin: 12px 0 0;
        background: #fff;
        border-radius: 5px;
        border: 1px solid rgb(229, 232, 235);
        display: flex;
        position: relative;
    }
    input {
        background: transparent;
        border: none;
        flex: 1 0 0%;
        height: 48px;
        outline: none;
        padding: 0 12px 0 12px;
        overflow-x: hidden;

        margin: 0;
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }
`;

const CollecionUrlContainer = styled(CollecionNameContainer)`
    p {
        align-items: center;
        background: transparent;
        color: rgb(138, 147, 155);
        display: flex;
        padding-left: 12px;
    }
    input {
        padding: 0 12px 0 0;
    }
`;
const CollecionDescriptionContainer = styled(CollecionNameContainer)`
    textarea {
        margin: 12px 0 0;
        background: #fff;
        border-radius: 5px;
        border: 1px solid rgb(229, 232, 235);
        width: 100%;
        height: auto;
        padding: 16px 12px;
        resize: vertical;
        outline: none;
    }
`;
const CollectionCategoryContainer = styled(CollecionNameContainer)`
    select {
        border-color: #6c757d;
        border: 1px solid #6c757d;

        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        border-radius: 0.25rem;
        &:hover {
            cursor: pointer;
            background-color: #6c757d;
            border: 1px solid transparent;
            color: #fff;
        }
    }
`;
const CollectionLinkContainer = styled(CollecionNameContainer)`
    width: 740px;
    div {
        margin-bottom: 4px;
        position: relative;
        display: flex;
        flex-wrap: wrap;
        width: 100%;
        margin-left: -1px;
        height: 34px;
        margin-top: 0;
        span {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: center;
            white-space: nowrap;
            background-color: #e9ecef;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            height: 34px;
        }
        input {
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            height: 34px;
        }
    }
`;
const CollectionBtnContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 20px;
    margin-bottom: 100px;
    width: 740px;
`;
const CollectionSubmitBtn = styled.button`
    background-color: #e8632e;
    border: 1px solid #e8632e;
    padding: 0.5rem 1rem;
    font-size: 1.25rem;
    border-radius: 0.3rem;
    color: #fff;
    &:hover {
        cursor: pointer;
    }
`;
const CollectionDeleteBtn = styled(CollectionSubmitBtn)`
    color: #fff;
    background-color: #dc3545;
    border-color: #dc3545;
    &:hover {
        background-color: #bb2d3b;
        border-color: #b02a37;
    }
`;
const index: NextPage = () => {
    return (
        <>
            <Layout />
            <BodyContainer>
                <CollectionMenu>
                    <a href="/collection">
                        <BsFillCollectionFill size="24" />
                        <p>My Collections</p>
                    </a>
                </CollectionMenu>
                <CollectionEditContainer>
                    <CollectionEditTop>
                        <a href="/collection">
                            <BsChevronLeft size="20" />
                            Back to Soung jehun
                        </a>
                    </CollectionEditTop>
                    <CollectionEditBodyContainer>
                        <h1>Edit your collection</h1>
                        <CollectionBodyItemContainer>
                            <h5>Logo image</h5>
                            <h6>
                                This image will also be used for navigation. 300
                                x 300 recommended.
                            </h6>
                            <CollectionBodyItemImgContainer>
                                <img src="https://nftmania.io/uploads/collection/collection_60cc3ce85aabd.png" />
                                <div>
                                    <input
                                        type="file"
                                        name="collection_image"
                                    />
                                </div>
                            </CollectionBodyItemImgContainer>
                        </CollectionBodyItemContainer>
                        <CollectionBodyItemContainer>
                            <h5>Featured image</h5>
                            <h6>
                                (optional) This image will be used for featuring
                                your collection on the homepage, category pages,
                                or other promotional areas of NFTMANIA. 600 x
                                400 recommended.
                            </h6>
                            <CollectionFeaturedImageContainer>
                                <img />
                                <div>
                                    <input
                                        type="file"
                                        name="collection_image"
                                    />
                                </div>
                            </CollectionFeaturedImageContainer>
                        </CollectionBodyItemContainer>
                        <CollecionNameContainer>
                            <h5>Name</h5>
                            <div>
                                <input defaultValue="Soung jehun" />
                            </div>
                        </CollecionNameContainer>
                        <CollecionUrlContainer>
                            <h5>URL</h5>
                            <h6>
                                Customize your URL on NFTMANIA. Must only
                                contain lowercase letters, numbers, and hyphens.
                            </h6>
                            <div>
                                <p>https://nftmania.io/assetlist/</p>
                                <input defaultValue="soung-jehun" />
                            </div>
                        </CollecionUrlContainer>
                        <CollecionDescriptionContainer>
                            <h5>Description</h5>
                            <h6>4 of 1000 characters used.</h6>
                            <textarea
                                rows={4}
                                maxLength={1000}
                                defaultValue="test"
                            />
                        </CollecionDescriptionContainer>
                        <CollectionCategoryContainer>
                            <h5>Category</h5>
                            <h6>
                                Adding a category will help make your item
                                discoverable on NFTMANIA.
                            </h6>
                            <select defaultValue="Add category">
                                <option value="1">ART</option>
                                <option value="2">ILLUST</option>
                                <option value="3">ENTERTAINMENT</option>
                                <option value="4">SPORTS</option>
                                <option value="5">DOMAIN</option>
                                <option value="6">UNIVERSE</option>
                                <option value="7">FREE MARKET</option>
                            </select>
                        </CollectionCategoryContainer>
                        <CollectionLinkContainer>
                            <h5>Links</h5>
                            <div>
                                <span>
                                    <AiOutlineInstagram />
                                </span>
                                <input type="text" placeholder="Instagram" />
                            </div>
                            <div>
                                <span>
                                    <IoLogoFacebook />
                                </span>
                                <input type="text" placeholder="Facebook" />
                            </div>
                            <div>
                                <span>
                                    <IoLogoDiscord />
                                </span>
                                <input type="text" placeholder="Discord" />
                            </div>
                            <div>
                                <span>
                                    <IoLogoTwitter />
                                </span>
                                <input type="text" placeholder="Twitter" />
                            </div>
                            <div>
                                <span>
                                    <BiAt />
                                </span>
                                <input type="text" placeholder="Homepage" />
                            </div>
                            <div>
                                <span>
                                    <FaTelegram />
                                </span>
                                <input type="text" placeholder="Telegram" />
                            </div>
                        </CollectionLinkContainer>
                        <CollectionBtnContainer>
                            <CollectionSubmitBtn>
                                Submit Changes
                            </CollectionSubmitBtn>
                            <CollectionDeleteBtn>
                                Delete Collection
                            </CollectionDeleteBtn>
                        </CollectionBtnContainer>
                    </CollectionEditBodyContainer>
                </CollectionEditContainer>
            </BodyContainer>
        </>
    );
};

export default index;
