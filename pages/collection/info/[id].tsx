import { NextPage } from 'next';
import React from 'react';
import {
    BsChevronLeft,
    BsCollection,
    BsFillCollectionFill,
    BsPencil,
    BsSearch,
} from 'react-icons/bs';
import styled from 'styled-components';
import Layout from '../../../components/layout/NavBar';

const BodyContainer = styled.div`
    display: flex;
    flex-direction: row;
`;

const CollectionMenu = styled.div`
    padding: 32px 16px;
    display: flex;
    position: fixed;
    width: 300px;
    height: 100%;
    border-right: 1px solid rgb(229, 232, 235);

    a {
        width: 270px;
        padding: 12px 16px;
        display: flex;
        position: fixed;
        background-color: rgb(251, 253, 255);
        align-items: center;
        color: rgba(53, 56, 64, 0.75);
        svg {
            font-size: 24px;
            margin-right: 10px;
            font-weight: 500;
        }
        &:hover {
            background-color: rgb(229, 232, 235);
            color: rgb(47, 63, 78);
            cursor: pointer;
        }
    }
`;

const CollectionDetailContainer = styled.div`
    margin-left: 300px;
    vertical-align: top;
`;
const CollectionDetailTop = styled.div`
    display: flex;
    align-items: center;
    background: rgb(251, 253, 255);
    border-bottom: 1px solid rgb(229, 232, 235);
    height: 72px;
    padding: 0 8px;
    position: fixed;
    width: 100%;
    z-index: 4;
    a {
        color: rgb(136, 136, 136);
        font-size: 14px;
        display: flex;
        align-items: center;
    }
    svg {
        margin-right: 10px;
    }
`;
const CollectionBannerContainer = styled.div`
    padding-top: 72px;
    display: flex;
    width: 100%;
    justify-content: center;
    align-items: center;
    min-height: 120px;
    height: 372px;
    overflow: hidden;
    position: relative;
    background: rgb(229, 232, 235);
    img {
        object-fit: cover;
        height: 100%;
        width: 100%;
        transition: opacity 400ms ease 0s;
    }
    div {
        position: absolute;
        top: 87px;
        right: 15px;
        z-index: 1;
        width: 50px;
        height: 50px;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: #fff;
        border-radius: 5px;

        input {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
        }
    }
`;

const CollectionItemBodyContainer = styled.div`
    margin: 32px 54px;
`;
const CollectionItemHeader = styled.div`
    display: flex;
    margin: 0px 8px 32px;
`;
const CollectionItemHeaderImgContainer = styled.div`
    width: 215px;
    height: 215px;
    margin-right: 24px;
    img {
        width: 215px;
        height: 215px;
        object-fit: cover;
        vertical-align: middle;
    }
`;
const CollectionItemHeaderInfoContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    h2 {
        font-size: 40px;
        font-weight: 500;
    }
    h3 {
        font-size: 12px;
        margin-top: 8px;
        color: rgb(117, 118, 123);
        max-width: 65vw;
        overflow: auto;
    }
    h4 {
        margin-top: 17px;
        margin-bottom: 12px;
        flex: 1 0 0%;
    }
    div {
        display: flex;
        margin-top: 12px;
        a {
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 5px;
            border: 1px solid rgb(229, 232, 235);
            width: 64px;
            height: 64px;
            margin-right: 12px;
            flex-direction: column;
            color: rgb(138, 147, 155);
            font-size: 14px;
            &:hover {
                box-shadow: rgb(28 31 39 / 25%) 0px 0px 8px;
                color: rgb(53, 56, 64);
            }
        }
    }
`;
const CollectionItemContainer = styled.div`
    display: block;
    height: 90px;
    div {
        font-weight: bold;
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        margin: 15px 8px;
        svg {
            margin: 15px 8px 15px 0;
            vertical-align: middle;
        }
    }
    a {
        margin-top: 8px;
        margin-left: 8px;
        font-size: 17px;
        background-color: #e8632e;
        border: 1px solid #e8632e;
        padding: 0.5rem 1rem;
        border-radius: 0.3rem;
        color: #fff;
        cursor: pointer;
    }
`;
const CollectionItemListContainer = styled.div`
    display: flex;
    padding: 30px;
    flex-direction: column;
`;
const CollectionItemListSearchContainer = styled.div`
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;
    padding: 0px 12px 30px;
    div {
        width: 70%;
        height: 40px;
        position: relative;
        display: flex;
        flex-wrap: wrap;
        align-items: stretch;
        span {
            display: flex;
            align-items: center;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: center;
            white-space: nowrap;
            background-color: #e9ecef;
            border: 1px solid #ced4da;
            border-radius: 0.25rem 0 0 0.25rem;
        }
        input {
            margin-left: -1px;
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            position: relative;
            flex: 1 1 auto;
            width: 1%;
            min-width: 0;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0 0.25rem 0.25rem 0;
        }
    }
    select {
        margin-bottom: 1rem;
        width: 28%;
        height: 40px;
        padding-top: 0.25rem;
        padding-bottom: 0.25rem;
        padding-left: 0.5rem;
        font-size: 0.875rem;
        border-radius: 0.25rem;
        background-color: #fff;
        border: 1px solid #ced4da;
        option {
            font-weight: normal;
            display: block;
            white-space: nowrap;
            min-height: 1.2em;
            padding: 0px 2px 1px;
        }
    }
`;
const CollectionItemList = styled.div`
    font-weight: normal;
    display: block;
    white-space: nowrap;
    min-height: 1.2em;
    padding: 0px 2px 1px;
`;
const index: NextPage = () => {
    return (
        <>
            <Layout />
            <BodyContainer>
                <CollectionMenu>
                    <a href="/collection">
                        <BsFillCollectionFill size="24" />
                        <p>My Collections</p>
                    </a>
                </CollectionMenu>
                <CollectionDetailContainer>
                    <CollectionDetailTop>
                        <a href="/collection">
                            <BsChevronLeft size="20" />
                            Back to my collections
                        </a>
                    </CollectionDetailTop>
                    <CollectionBannerContainer>
                        <img src="https://nftmania.io/views/_layout/bootstrap/images/sample/bg_sample9.png" />
                        <div>
                            <BsPencil size="20" color="gray" />
                            <input type="file" />
                        </div>
                    </CollectionBannerContainer>
                    <CollectionItemBodyContainer>
                        <CollectionItemHeader>
                            <CollectionItemHeaderImgContainer>
                                <img src="https://nftmania.io/uploads/collection/collection_60cc3ce85aabd.png" />
                            </CollectionItemHeaderImgContainer>
                            <CollectionItemHeaderInfoContainer>
                                <h2>Soung jehun</h2>
                                <h3>
                                    0x7a92d4b067aacd8f85d00a7776290a343a2a5c28
                                </h3>
                                <h4>test</h4>
                                <div>
                                    <a href="/collection/edit/93">
                                        <BsPencil size="20" color="gray" />
                                        Edit
                                    </a>
                                </div>
                            </CollectionItemHeaderInfoContainer>
                        </CollectionItemHeader>

                        <CollectionItemContainer>
                            <div>
                                <BsCollection size="20" />
                                ITEMS
                            </div>
                            <a href="/create/add_decentral/93">
                                Create Decentral NFT(ETH)
                            </a>
                            <a href="/create/add_decentral/93">
                                Create Central NFT(ETH)
                            </a>
                            <a href="#">Add New Item (SYMBOL from XEM)</a>
                        </CollectionItemContainer>
                        <CollectionItemListContainer>
                            <CollectionItemListSearchContainer>
                                <div>
                                    <span>
                                        <BsSearch size="16" />
                                    </span>
                                    <input type="text" placeholder="search" />
                                </div>
                                <select defaultValue="Sort by">
                                    <option value="0">Sort by</option>
                                    <option value="1">Recently Listed</option>
                                    <option value="2">Recently Sold</option>
                                    <option value="3">Recently Created</option>
                                    <option value="4">Ending Soon</option>
                                </select>
                            </CollectionItemListSearchContainer>
                            <CollectionItemList></CollectionItemList>
                        </CollectionItemListContainer>
                    </CollectionItemBodyContainer>
                </CollectionDetailContainer>
            </BodyContainer>
        </>
    );
};

export default index;
