import { NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';
import Footer from '../components/layout/Footer';
import Layout from '../components/layout/NavBar';

const BodyContainer = styled.div`
    max-width: 1320px;
    margin: 0 auto;
    min-height: calc(100vh - 293px);
    margin-top: 50px;
`;
const BodyTopContainer = styled.div`
    img {
        width: 100%;
        vertical-align: middle;
    }
`;
const BodyMainContainer = styled.div`
    width: 100%;
    background-color: #f6f6f6;
    padding: 50px 50px 30px;
    h1 {
        font-size: 36px;
        font-weight: bold;
        padding-left: 15px;
        margin-bottom: 20px;
    }
`;
const BodyMainItemContainer = styled.div`
    width: calc(100%);
    background-color: #fff;
    border: 1px solid #d6dae2;
    border-radius: 20px;
    overflow: hidden;
`;

const BodyTitleContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    background: url('https://nftmania.io/views/_layout/bootstrap/images/schedule-june-bg.png')
        #e7e7e7 no-repeat right center;
    background-size: auto 100%;
    padding: 60px 40px;
    border-bottom: 1px solid #d6dae2;
    font-size: 22px;
    color: #000;
    font-weight: bold;
`;
const BodyItem = styled.div`
    position: relative;
    padding: 12px 0 12px 40px;
    border-bottom: 1px solid #d6dae2;
    &::before {
        position: absolute;
        top: 12px;
        left: 15px;
        content: '●';
        margin-right: 10px;
    }
`;
const BodyMainFooter = styled.div`
    display: flex;
    justify-content: space-around;
    align-items: center;
    background-color: #f6f6f6;
    border-top: 1px dashed #e3e3e3;
    border-bottom: 1px dashed #e3e3e3;
    padding: 30px 50px;
    img {
        height: 80px;
        margin-right: 50px;
        vertical-align: middle;
    }
`;
const Schedule: NextPage = () => {
    return (
        <>
            <Layout />
            <BodyContainer>
                <BodyTopContainer>
                    <img src="https://nftmania.io/views/_layout/bootstrap/images/schedule-pc-top.png" />
                </BodyTopContainer>
                <BodyMainContainer>
                    <h1>ENG</h1>
                    <BodyMainItemContainer>
                        <BodyTitleContainer>
                            <h1>NFT MANIA July auction schedule</h1>
                        </BodyTitleContainer>

                        <BodyItem>
                            <p>RAIZART Contest Award-winning work NFT</p>
                        </BodyItem>
                        <BodyItem>
                            <p>Yoo Jin Mo’s Pen Tip NFT</p>
                        </BodyItem>
                    </BodyMainItemContainer>
                </BodyMainContainer>
                <BodyMainFooter>
                    <img src="https://nftmania.io/views/_layout/bootstrap/images/nftmania-logo-main.png" />
                    <span>
                        NFTMANIA is leading the NFT ecosystem in Korea !
                        <br />
                        We'd like to ask for your concern in the future as well.
                        Thank you.
                    </span>
                </BodyMainFooter>
                <BodyMainContainer>
                    <h1>KOR</h1>
                    <BodyMainItemContainer>
                        <BodyTitleContainer>
                            <h1>NFT MANIA 7월 경매 일정</h1>
                        </BodyTitleContainer>

                        <BodyItem>
                            <p>RAIZART 공모전 수상작 NFT</p>
                        </BodyItem>
                        <BodyItem>
                            <p>유진모의 펜 끝 NFT</p>
                        </BodyItem>
                    </BodyMainItemContainer>
                </BodyMainContainer>
                <BodyMainFooter>
                    <img src="https://nftmania.io/views/_layout/bootstrap/images/nftmania-logo-main.png" />
                    <span>
                        대한민국의 NFT 생태계를 선도해 나가는 NFTMANIA !
                        <br />
                        앞으로도 많은 관심 부탁드립니다. 감사합니다.
                    </span>
                </BodyMainFooter>
            </BodyContainer>
            <Footer />
        </>
    );
};

export default Schedule;
