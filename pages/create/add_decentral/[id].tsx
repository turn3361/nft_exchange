import { NextPage } from 'next';
import React from 'react';
import { AiOutlineInstagram } from 'react-icons/ai';
import { BiAt } from 'react-icons/bi';
import { BsChevronLeft, BsFillCollectionFill } from 'react-icons/bs';
import { FaTelegram } from 'react-icons/fa';
import { IoLogoDiscord, IoLogoFacebook, IoLogoTwitter } from 'react-icons/io5';
import styled from 'styled-components';
import Layout from '../../../components/layout/NavBar';

const BodyContainer = styled.div`
    display: flex;
    flex-direction: row;
`;

const CollectionMenu = styled.div`
    padding: 32px 16px;
    display: flex;
    position: fixed;
    width: 300px;
    height: 100%;
    border-right: 1px solid rgb(229, 232, 235);

    a {
        width: 270px;
        padding: 12px 16px;
        display: flex;
        position: fixed;
        background-color: rgb(251, 253, 255);
        align-items: center;
        color: rgba(53, 56, 64, 0.75);
        svg {
            font-size: 24px;
            margin-right: 10px;
            font-weight: 500;
        }
        &:hover {
            background-color: rgb(229, 232, 235);
            color: rgb(47, 63, 78);
            cursor: pointer;
        }
    }
`;

const CollectionCreateContainer = styled.div`
    margin-left: 300px;
    vertical-align: top;
`;
const CollectionCreateTop = styled.div`
    display: flex;
    align-items: center;
    background: rgb(251, 253, 255);
    border-bottom: 1px solid rgb(229, 232, 235);
    height: 72px;
    padding: 0 8px;
    position: fixed;
    width: 100%;
    z-index: 4;
    a {
        color: rgb(136, 136, 136);
        font-size: 14px;
        display: flex;
        align-items: center;
    }
    svg {
        margin-right: 10px;
    }
`;

const CollectionCreateBodyContainer = styled.div`
    width: 1605px;
    display: flex;
    justify-content: center;
    padding-top: 100px;
`;
const CollectionItemContainer = styled.div`
    width: 600px;
    padding: 48px 0;
    h1 {
        font-size: 38px;
        font-weight: 900;
        line-height: 50px;
    }
    h2 {
        margin-top: 16px;
        color: rgba(4, 4, 5, 0.5);
        font-weight: 600;
        font-size: 17px;
    }
`;
const CollectionFormContainer = styled.div`
    display: flex;
    flex-basis: auto;
    align-items: flex-start;
    flex-direction: row;
    position: relative;
    margin-top: 32px;
`;
const CollectionFormItem = styled.div`
    flex: 1 1 auto;
    display: flex;
    flex-direction: column;
    align-items: stretch;
`;
const CollectionFileBoxContainer = styled.div`
    margin-top: 8px;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    div {
        padding: 32px 60px;
        border: 2px dashed rgba(4, 4, 5, 0.1);
        min-height: 140px;
        position: relative;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        border-radius: 16px;
        width: 100%;
        &:hover {
            border: 2px dashed rgba(4, 4, 5, 0.3);
            border-radius: 16px;
        }
        input {
            display: none;
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }
        img {
            width: 100%;
            vertical-align: middle;
        }
        p {
            color: rgba(4, 4, 5, 0.5);
            font-size: 14px;
            line-height: 21px;
            font-weight: 600;
            text-align: center;
        }
        label {
            margin-top: 16px;
            color: #e8632e;
            background: rgba(232, 99, 46, 0.15);
            padding: 10px 22px 11px;
            min-width: 160px;
            text-align: center;
            border-radius: 40px;
            font-size: 14px;
            font-weight: bold;
            appearance: button;
            &:hover {
                background: rgba(232, 99, 46, 0.3);
            }
        }
    }
    a {
        background-color: #e8632e;
        border: 1px solid #e8632e;
        color: #fff;
        border-radius: 0.25rem;
        text-align: center;
        cursor: pointer;
        text-decoration: none;
        font-weight: 400;
        padding: 0.2rem 0.3rem;
        font-size: 0.8rem;
        margin-left: 10px;
    }
`;
const CollectionNameContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    margin-bottom: 32px;
    h3 {
        margin-bottom: 0px;
        color: rgba(4, 4, 5, 0.9);
        font-size: 16px;
        line-height: 24px;
        font-weight: 400;
    }
    div {
        margin-top: 8px;
        display: flex;
        flex-direction: column;
        align-items: stretch;
        border: 1px solid rgba(4, 4, 5, 0.07);
        border-radius: 5px;
        padding: 10px 10px;
    }
    input {
        border: 0;
        width: 100%;
        outline: none;
        font-size: 15px;
        height: 20px;
    }
`;

const CollectionDescriptionContainer = styled(CollectionNameContainer)`
    h4 {
        padding-left: 5px;
        font-size: 13px;
        opacity: 0.7;
    }
    div {
        border: 1px solid rgba(4, 4, 5, 0.07);
        border-radius: 5px;
        padding: 10px 10px;
    }
    textarea {
        height: 150px;
        border: 0;
        width: 100%;
        outline: none;
        font-size: 15px;
    }
    p {
        margin-top: 3px;
        font-size: 14px;
        opacity: 0.7;
    }
`;

const CollectionPropertiesContainer = styled(CollectionDescriptionContainer)`
    span {
        display: flex;
        align-items: center;
    }
    div {
        flex-direction: row;
        justify-content: space-between;
        border: 0;
        height: 20px;
    }
    input {
        width: 48%;
        border: 1px solid rgba(4, 4, 5, 0.07);
        border-radius: 5px;
        padding: 20px;
        outline: none;
        font-size: 15px;
        height: 20px;
    }
`;
const CollectionPutOnSaleContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 32px;
    h3 {
        color: rgba(4, 4, 5, 0.9);
        font-size: 18px;
        line-height: 25px;
        font-weight: 700;
    }
    h4 {
        color: rgba(4, 4, 5, 0.5);
        font-size: 14px;
        line-height: 20px;
        font-weight: 500;
        margin-top: 4px;
    }
`;
const CollectionPutOnSaleBtnContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    margin: 0px 0 0 16px;
    flex-basis: auto;
    flex-shrink: 0;
    button {
        background: transparent;
        padding: 0;
        cursor: pointer;
        outline: none;
        border: 0;
        input {
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
            display: none;
        }
        label {
            display: flex;
            flex-basis: auto;
            flex-shrink: 0;
            align-items: center;
            div {
                position: relative;
                width: 40px;
                height: 20px;
                background: rgb(0, 102, 255);
                border-radius: 40px;
                transition: all 0.12s ease-in-out 0s;
                display: flex;
                align-items: stretch;
                flex-basis: auto;
                flex-direction: column;
                flex-shrink: 0;
                span {
                    background: #fff;
                    width: 12px;
                    height: 12px;
                    border-radius: 12px;
                    position: absolute;
                    transition: all 0.12s ease-in-out 0s;
                    top: 4px;
                    left: 4px;
                    transform: translateX(20px);
                    display: flex;
                    flex-basis: auto;
                    align-items: stretch;
                }
            }
        }
    }
`;
const CollectionSalePriceContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    margin-bottom: 32px;
`;
const CollectionSalePriceTop = styled.div`
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    align-items: stretch;
    h3 {
        color: rgba(4, 4, 5, 0.9);
        font-size: 18px;
        line-height: 25px;
        font-weight: 700;
    }
    h4 {
        color: rgba(4, 4, 5, 0.5);
        font-size: 14px;
        line-height: 20px;
        font-weight: 500;
        margin-top: 4px;
    }
`;
const CollectionSalePriceBody = styled.div`
    margin-top: 16px;
    align-items: stretch;
    flex-direction: column;
    div {
        border-bottom: 2px solid rgba(4, 4, 5, 0.07);
        padding: 14px 0;
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 10px;
        input {
            width: calc(100% - 100px);
            font-size: 15px;
            color: rgba(4, 4, 5, 0.8);
            background: transparent;
            border: 0;
            outline: none;
        }
        select {
            display: flex;
            width: 100px;
            padding: 0.375rem 2.25rem 0.375rem 0.75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
        }
    }
`;
const CollectionUnlockContainer = styled(CollectionPutOnSaleContainer)``;
const CollectionUnlockBtnContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    margin: 0px 0 0 16px;
    flex-basis: auto;
    flex-shrink: 0;
    button {
        background: transparent;
        padding: 0;
        cursor: pointer;
        outline: none;
        border: 0;
        input {
            display: none;
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
        }
        label {
            display: flex;
            flex-basis: auto;
            flex-shrink: 0;
            align-items: center;
            div {
                position: relative;
                width: 40px;
                height: 20px;
                background: rgba(0, 102, 255, 0.1);
                border-radius: 40px;
                transition: all 0.12s ease-in-out 0s;
                display: flex;
                align-items: stretch;
                flex-basis: auto;
                flex-direction: column;
                flex-shrink: 0;
                span {
                    background: rgb(0, 102, 255);
                    width: 12px;
                    height: 12px;
                    border-radius: 12px;
                    position: absolute;
                    transition: all 0.12s ease-in-out 0s;
                    top: 4px;
                    left: 4px;
                    transform: translateX(0px);
                    display: flex;
                    flex-basis: auto;
                    align-items: stretch;
                }
            }
        }
    }
`;
const CollectionUploadContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    margin-bottom: 32px;
    h3 {
        margin-bottom: 0px;
        color: rgba(4, 4, 5, 0.9);
        font-size: 16px;
        line-height: 24px;
        font-weight: bold;
    }
`;
const CollectionUploadImgContainer = styled.div`
    margin-top: 8px;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    div {
        display: flex;
        margin-top: 16px;
        flex-flow: row wrap;
        background: transparent;
        border: 1px solid rgb(0, 102, 255);
        width: 128px;
        height: 140px;
        flex-direction: column;
        border-radius: 16px;
        align-items: center;
        justify-content: center;
        transition: all 0.12s ease-in-out 0s;
        img {
            width: 40px;
            height: 40px;
            border-radius: 100px;
            vertical-align: middle;
        }
        span {
            margin-top: 8px;
            font-size: 14px;
            font-weight: bold;
            text-align: center;
        }
    }
`;
const CollectionModeContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    margin-bottom: 32px;
    h3 {
        margin-bottom: 0px;
        color: rgba(4, 4, 5, 0.9);
        font-size: 16px;
        line-height: 24px;
        font-weight: bold;
    }
`;
const CollectionModeInputContainer = styled.div`
    margin-top: 8px;
    display: flex;
    flex-direction: column;
    align-items: stretch;
`;
const CollectionModeCheckboxInputContainer = styled.div`
    display: block;
    min-height: 1.5rem;
    margin-bottom: 0.125rem;
    input {
        border-radius: 0.25em;
        width: 1em;
        height: 1em;
        margin-top: 0.25em;
        vertical-align: top;
        background-color: #fff;
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        border: 1px solid rgba(0, 0, 0, 0.25);
    }
`;
const CollectionModeTextInputContainer = styled.div`
    border: 1px solid rgba(4, 4, 5, 0.07);
    border-radius: 5px;
    padding: 10px 10px;
    width: 578px;
    width: 100%;
    input {
        width: 100%;
        border: 0;
    }
`;
const CollectionAuctionContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    margin-bottom: 32px;
`;
const CollectionAuctionBodyContainer = styled.div`
    margin-top: 8px;
    display: flex;
    flex-direction: column;
    align-items: stretch;
`;
const CollectionAuctionTop = styled.div`
    border: 1px solid rgba(4, 4, 5, 0.07);
    border-radius: 5px;
    padding: 10px 10px;
`;
const CollectionAuctionBody = styled.div`
    border: 1px solid rgba(4, 4, 5, 0.07);
    border-radius: 5px;
    padding: 10px 10px;
    display: flex;
    flex-direction: row;
    div {
        width: 50%;
    }
`;
const CollectionCreateButtonContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    margin-bottom: 32px;
    a {
        background-color: #e8632e;
        border: 1px solid #e8632e;
        margin-bottom: 0.25rem;
        color: #fff;
        font-weight: 400;
        line-height: 1.5;
        text-align: center;
        text-decoration: none;
        vertical-align: middle;
        cursor: pointer;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        border-radius: 0.25rem;
    }
`;
const index: NextPage = () => {
    return (
        <>
            <Layout />
            <BodyContainer>
                <CollectionMenu>
                    <a href="/collection">
                        <BsFillCollectionFill size="24" />
                        <p>My Collections</p>
                    </a>
                </CollectionMenu>
                <CollectionCreateContainer>
                    <CollectionCreateTop>
                        <a href="/collection">
                            <BsChevronLeft size="20" />
                            Back to Select Type
                        </a>
                    </CollectionCreateTop>
                    <CollectionCreateBodyContainer>
                        <CollectionItemContainer>
                            <h1>Create Decentral NFT(ETH)</h1>
                            <h2>
                                It is uploaded to the blockchain as soon as it
                                is created with a decentralized token.
                                <br />
                                Ethereum Network usage fee required to issue
                                token is incurred.
                                <br />
                                (If you select Decentral NFT, you will receive a
                                1% royalty for each transaction completed.)
                            </h2>
                            <h2>
                                탈중앙형 토큰으로 만드는 즉시 블럭체인상에
                                업로드 되며,
                                <br />
                                토큰 발행에 필요한 이더리움 네트웍 사용 수수료가
                                발생 합니다.
                                <br />
                                (Decentral NFT를 선택하신 경우 거래 완료건마다
                                발생하는 1%의 로열티는 받습니다.)
                            </h2>
                            <CollectionFormContainer>
                                <CollectionFormItem>
                                    <h3>Main file</h3>
                                    <CollectionFileBoxContainer>
                                        <div>
                                            <input
                                                type="file"
                                                name="file-upload"
                                            />
                                            <img src="" />
                                            <p>
                                                PNG, GIF, WEBP, MP4 or MP3. Max
                                                30mb.
                                            </p>
                                            <label>Choose File</label>
                                        </div>
                                        <br />
                                        <h3>
                                            Sub file ( You can register up to 10
                                            sub files. )<a>Add</a>
                                        </h3>
                                    </CollectionFileBoxContainer>
                                    <br />
                                    <CollectionNameContainer>
                                        <h3>Name</h3>
                                        <div>
                                            <input
                                                placeholder='"Product Name"'
                                                type="text"
                                            />
                                        </div>
                                    </CollectionNameContainer>
                                    <br />
                                    <CollectionDescriptionContainer>
                                        <h3>Description</h3>
                                        <h4>(Optional)</h4>
                                        <div>
                                            <textarea placeholder='"Please briefly explain this product."' />
                                        </div>
                                        <p>With preserved line-breaks</p>
                                    </CollectionDescriptionContainer>
                                    <br />
                                    <CollectionDescriptionContainer>
                                        <h3>Commission (Percent)</h3>
                                        <div>
                                            <input
                                                placeholder="0"
                                                max="3"
                                                min="0"
                                            />
                                        </div>
                                        <p>
                                            You collect a fee when you sell one
                                            of the products. This amount will be
                                            deducted from the final selling
                                            price and will be paid to the
                                            selected payment address.
                                        </p>
                                    </CollectionDescriptionContainer>
                                    <br />
                                    <CollectionPropertiesContainer>
                                        <span>
                                            <h3>Properties</h3>
                                            <h4>(Optional)</h4>
                                        </span>

                                        <div>
                                            <input placeholder="e. g. Size" />
                                            <input placeholder="e. g. M" />
                                        </div>
                                    </CollectionPropertiesContainer>
                                    <br />
                                    <CollectionPutOnSaleContainer>
                                        <div>
                                            <h3>Put on sale</h3>
                                            <h4>
                                                You’ll receive bids on this item
                                            </h4>
                                        </div>
                                        <CollectionPutOnSaleBtnContainer>
                                            <button>
                                                <input
                                                    type="checkbox"
                                                    value="Y"
                                                />
                                                <label>
                                                    <div>
                                                        <span />
                                                    </div>
                                                </label>
                                            </button>
                                        </CollectionPutOnSaleBtnContainer>
                                    </CollectionPutOnSaleContainer>
                                    <br />
                                    <CollectionSalePriceContainer>
                                        <CollectionSalePriceTop>
                                            <h3>Sale Price</h3>
                                            <h4>
                                                Enter the price at which the
                                                product will be sold.
                                                <br />* The sale price will be
                                                charged when the auction is in
                                                progress.
                                            </h4>
                                        </CollectionSalePriceTop>
                                        <CollectionSalePriceBody>
                                            <div>
                                                <input defaultValue="0" />
                                                <select defaultValue="ETH">
                                                    <option value="ETH">
                                                        ETH
                                                    </option>
                                                </select>
                                            </div>
                                        </CollectionSalePriceBody>
                                    </CollectionSalePriceContainer>
                                    <br />
                                    <CollectionUnlockContainer>
                                        <div>
                                            <h3>Unlock once purchased</h3>
                                            <h4>
                                                Content will be unlocked after
                                                successful transaction
                                            </h4>
                                        </div>
                                        <CollectionUnlockBtnContainer>
                                            <button>
                                                <input
                                                    type="checkbox"
                                                    value="Y"
                                                />
                                                <label>
                                                    <div>
                                                        <span />
                                                    </div>
                                                </label>
                                            </button>
                                        </CollectionUnlockBtnContainer>
                                    </CollectionUnlockContainer>
                                    <br />
                                    <CollectionUploadContainer>
                                        <h3>Collection to be uploaded</h3>
                                        <CollectionUploadImgContainer>
                                            <div>
                                                <img src="https://nftmania.io/uploads/collection/collection_60cc3ce85aabd.png" />
                                                <span>Soung jehun</span>
                                            </div>
                                        </CollectionUploadImgContainer>
                                    </CollectionUploadContainer>
                                    <br />
                                    <CollectionModeContainer>
                                        <h3>The mode of Sale</h3>
                                        <CollectionModeInputContainer>
                                            <CollectionModeCheckboxInputContainer>
                                                <input type="checkbox" />
                                                <label>Direct Sale</label>
                                            </CollectionModeCheckboxInputContainer>
                                            <CollectionModeCheckboxInputContainer>
                                                <input type="checkbox" />
                                                <label>Offer Sale</label>
                                            </CollectionModeCheckboxInputContainer>
                                            <CollectionModeCheckboxInputContainer>
                                                <input type="checkbox" />
                                                <label>
                                                    Highest Price Auction
                                                </label>
                                            </CollectionModeCheckboxInputContainer>
                                            <CollectionModeTextInputContainer>
                                                <input
                                                    placeholder='"Bidding Increase Price"'
                                                    type="text"
                                                />
                                            </CollectionModeTextInputContainer>
                                        </CollectionModeInputContainer>
                                    </CollectionModeContainer>
                                    <br />
                                    <CollectionAuctionContainer>
                                        <h3>Auction Deadline</h3>
                                        <CollectionAuctionBodyContainer>
                                            <CollectionAuctionTop>
                                                <label>
                                                    <input type="checkbox" />
                                                    Until successful bidder
                                                    selection
                                                </label>
                                            </CollectionAuctionTop>
                                            <CollectionAuctionBody>
                                                <div>2021-07-19</div>
                                                <div>12:16 PM</div>
                                            </CollectionAuctionBody>
                                        </CollectionAuctionBodyContainer>
                                    </CollectionAuctionContainer>
                                    <br />
                                    <CollectionCreateButtonContainer>
                                        <a>Create item</a>
                                    </CollectionCreateButtonContainer>
                                </CollectionFormItem>
                            </CollectionFormContainer>
                        </CollectionItemContainer>
                    </CollectionCreateBodyContainer>
                </CollectionCreateContainer>
            </BodyContainer>
        </>
    );
};

export default index;
