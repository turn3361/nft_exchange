import React, { useEffect, useRef } from 'react';
import { NextPage } from 'next';
import Head from 'next/Head';
import styled from 'styled-components';
import SwiperCore, {
    Navigation,
    Pagination,
    Scrollbar,
    A11y,
    Autoplay,
} from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { AiOutlineClockCircle } from 'react-icons/ai';
import Layout from '../components/layout/NavBar';
import Footer from '../components/layout/Footer';
import { useDispatch } from 'react-redux';
import { login } from '../actions/UserInfo';
import Link from 'next/link';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Autoplay]);

const Navcontainer = styled.div`
    position: sticky;
    top: 173px;
    width: 100%;
    border-bottom: 1px solid #e5e5e5;
    background-color: #fff;
    height: 48px;
    z-index: 2;
`;

const NavUl = styled.ul`
    list-style: none;
    height: 100%;
    padding: 0;
    margin: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    overflow-x: auto;
    font-size: 14px;
    li {
        height: 100%;
        width: 168px;
        display: flex;
        justify-content: center;
        align-items: center;
        &:hover {
            cursor: pointer;
            color: #0d6efd;
        }
    }
    div {
        content: '';
        border-right: 1px solid #e5e5e5;
        height: 40%;
        margin: 0;
        padding: 0;
    }
`;

const MainBodyContainer = styled.div`
    margin: 18px auto;
    width: 68%;
`;

const TopBannerImg = styled.img`
    width: 100%;
    height: 348px;
`;
const MidBannerImg = styled.img`
    width: 100%;
    height: 315px;
    margin-bottom: 35px;
    padding-bottom: 5px;
`;
const MidHowToSection = styled.div`
    display: flex;
    flex-direction: row;
    margin-top: 30px;
    justify-content: space-between;
    img {
        width: 643px;
        height: 103px;
        &:hover {
            cursor: pointer;
        }
    }
`;

const RecommendContainer = styled.div`
    display: flex;
    flex-direction: 'row';
`;

const RecommendTextContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: flex-start;
    flex-direction: column;
    font-size: 24px;
    font-weight: 300;
    width: 300px;
`;
const ReconmendContent = styled.div`
    border: 1px solid #dfdfdf;
    margin-left: 15px;
    width: 230px;
    border-radius: 5px;
    img {
        width: 100%;
    }
    p {
        border-top: 1px solid #dfdfdf;
        border-bottom: 1px solid #dfdfdf;
        height: 48px;
        text-align: center;
        padding: 12px 20px;
    }
`;

const CollectblesImg = styled.img`
    width: 100%;
    height: 270px;
`;

const CollectiblesContainer = styled.div`
    margin-top: 100px;
`;

const CollectiblesTopText = styled.div`
    display: flex;
    justify-content: space-between;

    p {
        font-size: 24px;
        font-weight: 400;
    }
    a {
        margin-right: 10px;
        color: #0d6efd;
    }
`;

const CollectiblesTopViewcontainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

const CollectiblesPrevButton = styled.div`
    display: flex;
    width: 30px;
    height: 30px;
    border: 1px solid #dfdfdf;
    border-radius: 100%;
    align-items: center;
    justify-content: center;
    &::after {
        content: '<';
        font-size: 20px;
        font-weight: 200;
    }
`;
const CollectiblesNextButton = styled.div`
    display: flex;
    width: 30px;
    height: 30px;
    border: 1px solid #dfdfdf;
    border-radius: 100%;
    align-items: center;
    justify-content: center;
    &::after {
        content: '>';
        font-size: 20px;
        font-weight: 200;
    }
`;

const CollectiblesSwiperCardcontainer = styled.div`
    border: 1px solid #dfdfdf;
    border-radius: 5px;
    margin-left: 10px;
    p {
        font-size: 12px;
    }
`;
const CollectiblesSwiperCardTextcontainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: 10px 10px 0px 10px;

    img {
        width: 20px;
        height: 20px;
    }
`;

const CollectiblesSwiperCardTopText = styled.div`
    font-size: 11px;
    color: rgba(46, 54, 59, 0.6);
`;

const CollectiblesSwiperCardPriceContinner = styled.div`
    margin: 10px;
`;

const CollectiblesSwiperCardDatecontainer = styled.div`
    margin: 10px 10px 0px 10px;
    font-size: 11px;
    display: flex;
    align-items: center;
`;

const CollectiblesSwiperCardButtoncontainer = styled.div`
    display: flex;
    justify-content: center;
    margin-top: 5px;

    button {
        width: 207px;
        height: 20px;
        margin-bottom: 10px;
        border: 1px solid #e8632e;
        border-radius: 5px;
        background-color: #e8632e;
        color: white;
        font-size: 11px;
        align-items: center;
    }
`;

const Noticecontainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin: 100px auto 20px auto;
    width: 68%;

    p {
        font-size: 20px;
        font-weight: 700;
    }
    a {
        margin-right: 10px;
        color: #0d6efd;
    }
`;

const NoticeUnderLine = styled.div`
    width: 100%;
    display: flex;
    justify-content: flex-start;
    border-bottom: 1px solid #dfdfdf;
    content: ' ';
`;

const NoticeListContainer = styled.div`
    margin: 0 auto;
    width: 68%;
`;

const NoticeList = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    margin-top: 18px;
    padding-bottom: 18px;
    border-bottom: 1px solid #dfdfdf;
    p {
        margin-right: 15px;
    }
`;
const Index: NextPage = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(login());
    }, []);

    const navList = [
        'ALL',
        'ART',
        'ILLUST',
        'ENTERTAINMENT',
        'SPORTS',
        'DOMAIN',
        'UNIVERSE',
        'FREE MARKET',
    ];
    const CollectiblesPrevRef = useRef<HTMLDivElement>(null);
    const CollectiblesNextRef = useRef<HTMLDivElement>(null);

    const navHandler = (name: string, index: number) => {
        return (
            <>
                <Link
                    href={{
                        pathname: '/assetlist',
                        query: { routeName: name },
                    }}
                    as="/assetlist"
                >
                    <li key={index}>{name}</li>
                </Link>
                <div />
            </>
        );
    };

    const topBannerImgHandler = (uri: string) => {
        return (
            <SwiperSlide>
                <TopBannerImg src={uri} />
            </SwiperSlide>
        );
    };

    const midBannerImgHandler = (uri: string) => {
        return (
            <SwiperSlide>
                <MidBannerImg src={uri} />
            </SwiperSlide>
        );
    };

    const reconmendContentHandler = (name: string, uri: string) => {
        return (
            <SwiperSlide>
                <ReconmendContent>
                    <img src={uri} />
                    <p>{name}</p>
                </ReconmendContent>
            </SwiperSlide>
        );
    };

    const collectiblesSwiperSlideHandler = (
        imgUri: string,
        topText: string,
        price: string,
        date: string,
    ) => {
        return (
            <SwiperSlide>
                <CollectiblesSwiperCardcontainer>
                    <CollectblesImg src={imgUri} />
                    <CollectiblesSwiperCardTextcontainer>
                        <div>
                            <CollectiblesSwiperCardTopText>
                                {topText}
                            </CollectiblesSwiperCardTopText>
                            <p>{topText}</p>
                        </div>
                        <img src="https://dev.nftmania.io/views/_layout/bootstrap/images/chainCentral.png" />
                    </CollectiblesSwiperCardTextcontainer>
                    <CollectiblesSwiperCardPriceContinner>
                        <CollectiblesSwiperCardTopText>
                            Price
                        </CollectiblesSwiperCardTopText>
                        <p>{price}</p>
                    </CollectiblesSwiperCardPriceContinner>
                    <CollectiblesSwiperCardDatecontainer>
                        <AiOutlineClockCircle />
                        {date}
                    </CollectiblesSwiperCardDatecontainer>
                    <CollectiblesSwiperCardButtoncontainer>
                        <button>On Auction</button>
                    </CollectiblesSwiperCardButtoncontainer>
                </CollectiblesSwiperCardcontainer>
            </SwiperSlide>
        );
    };

    const noticeListHandler = (text: string, date: string) => {
        return (
            <NoticeList>
                <div>{text}</div>
                <p>{date}</p>
            </NoticeList>
        );
    };
    return (
        <>
            <Head>
                <title>NFT Mania</title>
                <meta charSet="utf-8" />
                <meta
                    name="viewport"
                    content="initial-scale=1.0, width=device-width"
                />
            </Head>
            <Layout />
            <Navcontainer>
                <NavUl>
                    {navList.map((el, index) => navHandler(el, index))}
                </NavUl>
            </Navcontainer>

            <MainBodyContainer>
                <Swiper
                    className="swiper-container"
                    spaceBetween={0}
                    slidesPerView={1}
                    navigation
                    loop
                    autoHeight={true}
                    centeredSlides={true}
                    autoplay={{ delay: 3000 }}
                >
                    {topBannerImgHandler(
                        'https://nftmania.io/views/_layout/bootstrap/images/mainbanner-light.png',
                    )}
                    {topBannerImgHandler(
                        'https://nftmania.io/views/_layout/bootstrap/images/210513_nftmaria-web-main-banner_spo.png',
                    )}
                    {topBannerImgHandler(
                        'https://nftmania.io/views/_layout/bootstrap/images/0525-riaa-banner.png',
                    )}
                </Swiper>

                <Swiper
                    className="swiper-container"
                    spaceBetween={10}
                    slidesPerView={2}
                    pagination={true}
                    loop
                    autoHeight={true}
                    autoplay={{ delay: 2000 }}
                    style={{ marginTop: 30 }}
                >
                    {midBannerImgHandler(
                        'https://nftmania.io/views/_layout/bootstrap/images/centered-bn-8.png',
                    )}
                    {midBannerImgHandler(
                        'https://nftmania.io/views/_layout/bootstrap/images/centered-bn-5.png',
                    )}
                    {midBannerImgHandler(
                        'https://nftmania.io/views/_layout/bootstrap/images/centered-bn-12.png',
                    )}
                </Swiper>

                <MidHowToSection>
                    <img src="https://nftmania.io/views/_layout/bootstrap/images/main-guide-img1.png" />
                    <img src="https://nftmania.io/views/_layout/bootstrap/images/main-guide-img2.png" />
                </MidHowToSection>

                <RecommendContainer>
                    <RecommendTextContainer>
                        <div>RECOMMEND</div> <div>COLLECTIONS</div>
                    </RecommendTextContainer>
                    <Swiper
                        className="recommend-swiper-container"
                        slidesPerView={4}
                        navigation={true}
                        loop={false}
                        autoHeight={true}
                        style={{
                            marginTop: 30,
                            width: '100%',
                        }}
                    >
                        {reconmendContentHandler(
                            'VALENTINO',
                            'https://nftmania.io/uploads/cache/collection/thumb-collection_60a33a18a5f77_250x250.png',
                        )}
                        {reconmendContentHandler(
                            'MONCLER',
                            'https://nftmania.io/uploads/cache/collection/thumb-collection_60a33fbc2a71a_250x250.png',
                        )}
                        {reconmendContentHandler(
                            'Crypto eth',
                            'https://nftmania.io/uploads/cache/collection/thumb-collection_608d0aae20fdb_250x250.jpg',
                        )}
                        {reconmendContentHandler(
                            'PYLON FIGUREHEADS',
                            'https://nftmania.io/uploads/cache/collection/thumb-collection_6089936d816a1_250x250.jpg',
                        )}
                        {reconmendContentHandler(
                            'KANG RINA',
                            'https://nftmania.io/uploads/cache/collection/thumb-collection_608b5c6999012_250x250.jpg',
                        )}
                    </Swiper>
                </RecommendContainer>

                <CollectiblesContainer>
                    <CollectiblesTopText>
                        <p>COLLECTIBLES</p>
                        <CollectiblesTopViewcontainer>
                            <a href="#">VIEW ALL</a>
                            <CollectiblesPrevButton ref={CollectiblesPrevRef} />
                            <CollectiblesNextButton ref={CollectiblesNextRef} />
                        </CollectiblesTopViewcontainer>
                    </CollectiblesTopText>
                    <Swiper
                        className="collectbles-swiper-container"
                        slidesPerView={5}
                        spaceBetween={20}
                        pagination={false}
                        navigation={{
                            prevEl: CollectiblesPrevRef.current
                                ? CollectiblesPrevRef.current
                                : undefined,
                            nextEl: CollectiblesNextRef.current
                                ? CollectiblesNextRef.current
                                : undefined,
                        }}
                        loop={false}
                        autoHeight={true}
                        style={{ marginTop: 30 }}
                        onInit={(swiper) => {
                            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                            // @ts-ignore
                            // eslint-disable-next-line no-param-reassign
                            swiper.params.navigation.prevEl =
                                CollectiblesPrevRef.current;
                            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                            // @ts-ignore
                            // eslint-disable-next-line no-param-reassign
                            swiper.params.navigation.nextEl =
                                CollectiblesNextRef.current;
                            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                            // @ts-ignore
                            // eslint-disable-next-line no-param-reassign
                            swiper.navigation?.update();
                        }}
                    >
                        {collectiblesSwiperSlideHandler(
                            'https://nftmania.io/uploads/cache/product/thumb-product_608e90bdb47fe_350x.jpg',
                            'BAE SEUL GI',
                            '3,300 RUSH',
                            '2021-05-02',
                        )}
                        {collectiblesSwiperSlideHandler(
                            'https://nftmania.io/uploads/cache/product/thumb-product_608bb5ca76fa2_350x.jpg',
                            'BAE SEUL GI',
                            '3,300 RUSH',
                            '2021-05-02',
                        )}
                        {collectiblesSwiperSlideHandler(
                            'https://nftmania.io/uploads/cache/product/thumb-product_608b9e607c1b5_350x.jpg',
                            'BAE SEUL GI',
                            '3,300 RUSH',
                            '2021-05-02',
                        )}
                        {collectiblesSwiperSlideHandler(
                            'https://nftmania.io/uploads/cache/product/thumb-product_608de0bbd19ec_350x.jpg',
                            'BAE SEUL GI',
                            '3,300 RUSH',
                            '2021-05-02',
                        )}
                        {collectiblesSwiperSlideHandler(
                            'https://nftmania.io/uploads/cache/product/thumb-product_608bba9feeb08_350x.jpg',
                            'BAE SEUL GI',
                            '3,300 RUSH',
                            '2021-05-02',
                        )}
                        {collectiblesSwiperSlideHandler(
                            'https://nftmania.io/uploads/cache/product/thumb-product_608bba9feeb08_350x.jpg',
                            'BAE SEUL GI',
                            '3,300 RUSH',
                            '2021-05-02',
                        )}
                    </Swiper>
                </CollectiblesContainer>
            </MainBodyContainer>

            <Noticecontainer>
                <p>Notice</p>
                <a>Show All</a>
            </Noticecontainer>
            <NoticeUnderLine />
            <NoticeListContainer>
                {noticeListHandler(
                    '[Notice]Artist Contest Results Postpone Notice[공모전결과 연기 안내]',
                    '2021-06-03',
                )}
                {noticeListHandler(
                    '[Notice] Bidding System Check [입찰 시스템 점검]',
                    '2021-05-19',
                )}
            </NoticeListContainer>
            <Footer />
        </>
    );
};

export default Index;
