import { NextPage } from 'next';
import React from 'react';
import styled from 'styled-components';
import Layout from '../components/layout/NavBar';

const BodyContainer = styled.div`
    width: 68%;
    padding: 40px 0;
    margin: 0 auto;
    h1 {
        display: flex;
        justify-content: center;
        font-size: 32px;
        margin-bottom: 15px;
    }
    h2 {
        display: flex;
        justify-content: center;
        font-size: 20px;
    }
`;
const BodyRankingFilter = styled.div`
    margin-top: 30px;
    ul {
        display: flex;
    }
`;
const BodyRankingFilterLi = styled.li<{ border: boolean }>`
    margin-right: 30px;
    font-size: 16px;
    padding: 10px;
    border-bottom: ${(props) =>
        props.border ? `3px solid rgb(49, 148, 237)` : null};
    &:hover {
        cursor: pointer;
    }
`;
const BodyRankingList = styled.div`
    display: flex;
    flex-direction: column;
    overflow-x: scroll;
    width: 100%;
`;

const Ranking: NextPage = () => {
    return (
        <>
            <Layout />
            <BodyContainer>
                <h1>Top Non-Fungible Tokens</h1>
                <h2>
                    Volume, average, price and other top statistics for
                    non-fungible tokens (NFTs), updated hourly.
                </h2>
                <BodyRankingFilter>
                    <ul>
                        <BodyRankingFilterLi border={true}>
                            All
                        </BodyRankingFilterLi>
                        <BodyRankingFilterLi border={false}>
                            ART
                        </BodyRankingFilterLi>
                        <BodyRankingFilterLi border={false}>
                            ILLUST
                        </BodyRankingFilterLi>
                        <BodyRankingFilterLi border={false}>
                            ENTERTAINMENT
                        </BodyRankingFilterLi>
                        <BodyRankingFilterLi border={false}>
                            SPORTS
                        </BodyRankingFilterLi>
                        <BodyRankingFilterLi border={false}>
                            DOMAIN
                        </BodyRankingFilterLi>
                        <BodyRankingFilterLi border={false}>
                            UNIVERSE
                        </BodyRankingFilterLi>
                        <BodyRankingFilterLi border={false}>
                            FREE MARKET
                        </BodyRankingFilterLi>
                    </ul>
                </BodyRankingFilter>
                <BodyRankingList></BodyRankingList>
            </BodyContainer>
        </>
    );
};

export default Ranking;
