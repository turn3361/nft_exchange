import { NextPage } from 'next';
import React, { useState } from 'react';
import Layout from '../components/layout/NavBar';
import ModalComponent from '../components/account/Modal';
import Header from '../components/account/Header';
import InWallet from '../components/account/InWallet';
import Activity from '../components/account/Activity';
import Event from '../components/account/Event';
import Favorites from '../components/account/Favorites';
import Offer from '../components/account/Offer';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { RootStateInterface } from '../interfaces/RootState';
import { UserInfo } from '../interfaces/user/UserInfo.interface';
import SignIn from '../components/SignIn';

const Account: NextPage = () => {
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const { isLogin } = useSelector(
        (state: RootStateInterface): UserInfo => state.userInfo,
    );
    const route = useRouter();
    const sceneHandler = () => {
        switch (route.query.routeName) {
            case 'InWallet':
                return <InWallet />;
            case 'activity':
                return <Activity />;
            case 'event':
                return <Event />;
            case 'favorites':
                return <Favorites />;
            case 'offer':
                return <Offer />;
            default:
                return <InWallet />;
        }
    };
    return (
        <>
            <Layout />
            {isLogin && (
                <ModalComponent
                    modalIsOpen={modalIsOpen}
                    setModalIsOpen={setModalIsOpen}
                />
            )}
            {isLogin && (
                <Header
                    setModalIsOpen={setModalIsOpen}
                    selectNav={route.query?.routeName}
                />
            )}
            {isLogin && sceneHandler()}
            {!isLogin && <SignIn />}
        </>
    );
};
export default Account;
