import { NextPage } from 'next';
import React from 'react';
import { BiWallet } from 'react-icons/bi';
import { BsBucket, BsGearFill } from 'react-icons/bs';
import styled from 'styled-components';
import Footer from '../components/layout/Footer';
import Layout from '../components/layout/NavBar';

const BodyContainer = styled.div`
    display: flex;
    height: 100%;
    margin-bottom: -50px;
`;
const BodyMenuContainer = styled.div`
    align-self: flex-start;
    border-right: 1px solid rgb(229, 232, 235);
    flex-shrink: 0;
    height: calc(100vh - 153px);
    position: sticky;
    top: 64px;
    width: 380px;
    color: rgba(47, 63, 78, 0.75);
`;
const BodyMenuMywallet = styled.div`
    border-bottom: 1px solid rgb(229, 232, 235);
    color: rgb(4, 17, 29);
`;
const BodyMenuMywalletIconContainer = styled.div`
    color: rgba(47, 63, 78, 0.75);
    -webkit-box-align: center;
    align-items: center;
    display: flex;
    font-size: 16px;
    font-weight: 600;
    padding: 20px 12px;
    user-select: none;
    svg {
        margin-right: 10px;
    }
`;
const BodyMenuMywalletUserInfo = styled.div`
    border-top: 1px solid rgb(229, 232, 235);
    color: rgb(47, 63, 78);
    background: rgb(251, 253, 255);
    a {
        color: rgba(47, 63, 78, 0.75);
        padding: 20px;
        display: flex;
        align-items: center;
        -webkit-box-align: center;
        width: 100%;
        text-decoration: none;
        img {
            object-fit: cover;
            width: 32px;
            height: 32px;
            border-radius: 50%;
            margin-right: 10px;
            vertical-align: middle;
        }
    }
`;
const BodyMenuGeneralContainer = styled.div`
    background: rgb(237, 251, 255);
    pointer-events: none;
    color: rgb(4, 17, 29);
    cursor: pointer;
    font-size: 16px;
    font-weight: 600;
    padding: 10px 12px;
    display: flex;
    align-items: center;
    -webkit-box-align: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    a {
        color: rgb(4, 17, 29);
        width: 100%;
        padding: 10px 0;
        text-decoration: none;
        svg {
            margin-right: 10px;
            vertical-align: middle;
        }
    }
`;
const BodyMenuMyTokenContainer = styled.div`
    cursor: pointer;
    font-size: 16px;
    font-weight: 600;
    padding: 10px 12px;
    display: flex;
    align-items: center;
    -webkit-box-align: center;
    border-bottom: 1px solid rgb(229, 232, 235);
    a {
        color: rgba(47, 63, 78, 0.75);
        width: 100%;
        padding: 10px 0;
        text-decoration: none;
        svg {
            margin-right: 10px;
            vertical-align: middle;
        }
    }
`;
const BodyFormContainer = styled.form`
    display: flex;
    width: 100%;
    max-width: 800px;
    margin: 0;
    padding: 0;
`;
const BodyItemContainer = styled.div`
    max-width: 800px;
    margin: 0px 8vw;
    flex: 1 0 0%;
    h2 {
        margin: 60px 0 20px;
        font-size: 24px;
        font-weight: 600;
        line-height: 1.1;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    button {
        background-color: #e8632e;
        border: 1px solid #e8632e;
        margin-top: 1.5rem;
        padding: 0.5rem 1rem;
        font-size: 1.25rem;
        border-radius: 0.3rem;
        color: #fff;
        display: inline-block;
        font-weight: 400;
        line-height: 1.5;
        text-align: center;
        text-decoration: none;
        vertical-align: middle;
    }
`;
const BodyItem = styled.div`
    margin-bottom: 1.5rem;

    input {
        display: block;
        width: 100%;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
    }
    textarea {
        min-height: calc(1.5em + 0.75rem + 2px);
        display: block;
        width: 100%;
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
    }
`;
const BodyItemCheckEmailContainer = styled.div`
    margin-top: 20px;
    color: rgb(138, 147, 155);
    a {
        color: rgb(21, 178, 229);
        padding-left: 6px;
        text-decoration: none;
    }
`;
const Accountsettings: NextPage = () => {
    return (
        <>
            <Layout />
            <BodyContainer>
                <BodyMenuContainer>
                    <BodyMenuMywallet>
                        <BodyMenuMywalletIconContainer>
                            <BiWallet size="24" />
                            My Wallet
                        </BodyMenuMywalletIconContainer>
                        <BodyMenuMywalletUserInfo>
                            <a href="/accountwalletaddress">
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/user-sample-img.png" />
                                0x7a92.....2a5c28
                            </a>
                        </BodyMenuMywalletUserInfo>
                    </BodyMenuMywallet>
                    <BodyMenuGeneralContainer>
                        <a href="/accountsettings">
                            <BsGearFill size="24" />
                            General
                        </a>
                    </BodyMenuGeneralContainer>
                    <BodyMenuMyTokenContainer>
                        <a href="/accountmypoint">
                            <BsBucket size="24" />
                            My Tokens
                        </a>
                    </BodyMenuMyTokenContainer>
                </BodyMenuContainer>
                <BodyFormContainer>
                    <BodyItemContainer>
                        <h2>General Settings</h2>
                        <BodyItem>
                            <label>Username</label>
                            <br />
                            <br />
                            <input
                                placeholder="Enter Username"
                                defaultValue="60CC3B0A429EF"
                            />
                        </BodyItem>
                        <BodyItem>
                            <label>Bio</label>
                            <br />
                            <br />
                            <textarea placeholder="Tell the world your story !" />
                        </BodyItem>
                        <BodyItem>
                            <label>Email Address</label>
                            <br />
                            <br />
                            <input placeholder="Enter Email" />
                        </BodyItem>
                        <button type="submit">Save</button>
                        <BodyItemCheckEmailContainer>
                            Please check oooooo@google.com and verify your new
                            email address.
                            <br />
                            Still no email after a couple minutes?
                            <a href="#">Click here to resend.</a>
                        </BodyItemCheckEmailContainer>
                    </BodyItemContainer>
                </BodyFormContainer>
            </BodyContainer>
            <Footer />
        </>
    );
};

export default Accountsettings;
