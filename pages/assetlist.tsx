import { NextPage } from 'next';
import Router, { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import {
    AiFillStar,
    AiOutlineClockCircle,
    AiOutlineHeart,
} from 'react-icons/ai';
import { BiChevronDown, BiSearchAlt2 } from 'react-icons/bi';
import { BsFillCollectionFill, BsFillTagFill } from 'react-icons/bs';
import styled from 'styled-components';
import Layout from '../components/layout/NavBar';

const BodyContainer = styled.div`
    display: flex;
    flex-direction: row;
`;

const BodyLeftContainer = styled.div`
    flex: 4.5;
    border-right: 1px solid rgb(229, 232, 235);
`;

const NavBarContainer = styled.div`
    border-bottom: 1px solid rgb(229, 232, 235);
`;

const NavBarUl = styled.ul`
    list-style: none;
    display: flex;
    align-items: center;
    height: 66px;
    padding: 12px 10px;
`;

const NavBarLi = styled.li<{ select: boolean }>`
    padding: 10px 20px;
    border-radius: 5px;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 50px;
    margin-left: 10px;
    background-color: ${(props) =>
        props.select ? 'rgb(229, 232, 235)' : null};
    color: ${(props) =>
        props.select ? 'rgb(47, 63, 78)' : 'rgb(138, 147, 155)'};
    img {
        width: 30px;
        height: 30px;
        margin-right: 8px;
    }
    &:hover {
        background-color: rgb(229, 232, 235);
        color: rgb(47, 63, 78);
        cursor: pointer;
    }
`;

const ListBodyContainer = styled.div`
    margin: auto 31px;
`;

const ListCardContainer = styled.div`
    margin-top: 35px;
    display: grid;
    grid-template-columns: repeat(6, 230);
    grid-auto-rows: minmax(300px, auto);
    grid-gap: 20px;
`;
const ListCard = styled.div`
    width: 230px;
    height: 400px;
    border: 1px solid rgb(229, 232, 235);
    border-radius: 5px;
    background-color: #fff;

    &:hover {
        cursor: pointer;
        transition: all 0.1s ease 0s;
        box-shadow: rgb(47 63 78 / 25%) 0px 0px 8px 0px;
    }
`;
const ListCardHeader = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 8px;
    p {
        font-size: 13px;
        color: #aaa;
    }
`;
const ListCardHeaderRight = styled.div`
    display: flex;
    align-items: center;
    p {
        margin-left: 2px;
        font-size: 1rem;
        color: #212529;
    }
`;
const ListCardBodyContainer = styled.div`
    &:hover {
        cursor: pointer;
    }
`;

const ListCardImgContainer = styled.div`
    height: 260px;
    display: flex;
    justify-content: center;
    align-items: center;

    img {
        width: 100%;
        vertical-align: middle;
    }
`;

const ListCardBodyText = styled.div`
    border-top: 1px solid rgb(229, 232, 235);
    height: 100%;
    padding: 12px;
    p {
        font-size: 11px;
        color: rgb(46, 54, 59, 0.6);
        text-overflow: ellipsis;
        overflow: hidden;
        margin-bottom: 3px;
    }
    h5 {
        color: rgb(46, 54, 59);
        font-size: 16px;
        text-overflow: ellipsis;
        overflow: hidden;
        margin-top: 6px;
        margin-bottom: 4px;
    }
`;
const ListCardBodyTextTop = styled.div`
    display: flex;
    justify-content: space-between;

    img {
        width: 20px;
        height: 20px;
    }
    h6 {
        font-size: 13px;
        color: rgb(46, 54, 49);
        text-overflow: ellipsis;
        overflow: hidden;
        margin-top: 4px;
        margin-bottom: 5px;
    }
`;
const ListCardBodyTextBottom = styled.div`
    font-size: 11px;
    color: #000;
`;
const ListMoreButtonTextTop = styled.button`
    width: 96%;
    margin-left: 2%;
    background-color: #f8f9fa;
    color: #000;
    border-color: #f8f9fa;
    padding: 0.5rem 1rem;
    font-size: 1.25rem;
    border-radius: 0.3rem;
    margin-top: 10px;
    &:hover {
        cursor: pointer;
    }
`;

const BodyRightListDropContainer = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 13px 12px;
    border-bottom: 1px solid rgba(229, 232, 235);
    div {
        display: flex;
        align-items: center;
    }
    p {
        font-weight: 600;
        font-size: 16px;
        margin-left: 10px;
    }
    &:hover {
        cursor: pointer;
    }
`;
const BodyRightListDropStatusUl = styled.ul`
    border-bottom: 1px solid rgba(229, 232, 235);
    display: flex;
    flex-flow: wrap;
    padding: 20px;

    li {
        display: flex;
        text-align: center;
        justify-content: center;
        align-items: center;
        width: calc(50% - 8px);
        height: 50px;
        border: 1px solid rgb(229, 232, 235);
        border-radius: 5px;
        margin: 4px;
        padding: 10px;
        &:hover {
            cursor: pointer;
            box-shadow: rgb(47 63 78 / 25%) 0px 0px 8px 0px;
            transition: all 0.2s ease 0s;
        }
    }
`;

const CollectionsFilterContainer = styled.div`
    padding: 20px;
    display: flex;
    flex-flow: wrap;
    border-bottom: 1px solid rgba(229, 232, 235);
`;

const CollectionsFilter = styled.div`
    margin-bottom: 12px;
    border: 1px solid rgb(229, 232, 235);
    height: 45px;
    padding: 0 10px;
    border-radius: 5px;
    display: flex;
    align-items: center;
    flex: 1 0 0%;
    font-size: 18px;
    input {
        padding: 1px 10px;
    }
`;
const CollectionsFilterList = styled.ul`
    width: 100%;
    max-height: 207px;
    padding: 4px 0;
    display: flex;
    flex-direction: column;

    li {
        display: flex;
        align-items: center;
        color: rgb(47, 63, 78);
        height: 40px;
        padding: 0 8px;
        &:hover {
            cursor: pointer;
        }
    }
    img {
        display: flex;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        border: 1px solid rgb(229, 232, 235);
        justify-content: center;
        transition: all 400ms ease 0s;
        object-fit: cover;
        &:hover {
            transform: scale(1.1);
        }
    }
`;
const Assetilist: NextPage = () => {
    // navbar 상태값 정의
    const navInitState = {
        All: false,
        ART: false,
        ILLUST: false,
        ENTERTAINMENT: false,
        SPORTS: false,
        DOMAIN: false,
        UNIVERSE: false,
        FREEMARKET: false,
    };
    const [navState, setNavState] = useState(navInitState);
    // 라우터 사용
    const route = useRouter();
    //페이지 라우터 함수 실행
    useEffect(() => {
        navStateHandler();
    }, []);
    //라우터 이동시 상태값 변경
    const navStateHandler = () => {
        switch (route.query.routeName) {
            case 'ALL':
                return setNavState({ ...navInitState, All: true });
            case 'ART':
                return setNavState({ ...navInitState, ART: true });
            case 'ILLUST':
                return setNavState({ ...navInitState, ILLUST: true });
            case 'ENTERTAINMENT':
                return setNavState({ ...navInitState, ENTERTAINMENT: true });
            case 'SPORTS':
                return setNavState({ ...navInitState, SPORTS: true });
            case 'DOMAIN':
                return setNavState({ ...navInitState, DOMAIN: true });
            case 'UNIVERSE':
                return setNavState({ ...navInitState, UNIVERSE: true });
            case 'FREE MARKET':
                return setNavState({ ...navInitState, FREEMARKET: true });
            default:
                return setNavState({ ...navInitState, All: true });
        }
    };
    return (
        <>
            <Layout />
            <BodyContainer>
                <BodyLeftContainer>
                    <NavBarContainer>
                        <NavBarUl>
                            <NavBarLi
                                select={navState.All}
                                onClick={() =>
                                    setNavState({ ...navInitState, All: true })
                                }
                            >
                                All
                            </NavBarLi>
                            <NavBarLi
                                select={navState.ART}
                                onClick={() =>
                                    setNavState({ ...navInitState, ART: true })
                                }
                            >
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/1.art.png" />
                                ART
                            </NavBarLi>
                            <NavBarLi
                                select={navState.ILLUST}
                                onClick={() =>
                                    setNavState({
                                        ...navInitState,
                                        ILLUST: true,
                                    })
                                }
                            >
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/2.Illust.png" />
                                ILLUST
                            </NavBarLi>
                            <NavBarLi
                                select={navState.ENTERTAINMENT}
                                onClick={() =>
                                    setNavState({
                                        ...navInitState,
                                        ENTERTAINMENT: true,
                                    })
                                }
                            >
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/3.entertain.png" />
                                ENTERTAINMENT
                            </NavBarLi>
                            <NavBarLi
                                select={navState.SPORTS}
                                onClick={() =>
                                    setNavState({
                                        ...navInitState,
                                        SPORTS: true,
                                    })
                                }
                            >
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/4.sports.png" />
                                SPORTS
                            </NavBarLi>
                            <NavBarLi
                                select={navState.DOMAIN}
                                onClick={() =>
                                    setNavState({
                                        ...navInitState,
                                        DOMAIN: true,
                                    })
                                }
                            >
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/5.domain.png" />
                                DOMAIN
                            </NavBarLi>
                            <NavBarLi
                                select={navState.UNIVERSE}
                                onClick={() =>
                                    setNavState({
                                        ...navInitState,
                                        UNIVERSE: true,
                                    })
                                }
                            >
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/6.universepng.png" />
                                UNIVERSE
                            </NavBarLi>
                            <NavBarLi
                                select={navState.FREEMARKET}
                                onClick={() =>
                                    setNavState({
                                        ...navInitState,
                                        FREEMARKET: true,
                                    })
                                }
                            >
                                <img src="https://nftmania.io/views/_layout/bootstrap/images/7.freemarket.png" />
                                FREE MARKET
                            </NavBarLi>
                        </NavBarUl>
                    </NavBarContainer>

                    <ListBodyContainer>
                        <p style={{ fontSize: '1rem', marginTop: 3 }}>
                            408 results
                        </p>
                        <div>
                            <ListCardContainer>
                                <ListCard>
                                    <ListCardHeader>
                                        <p>Direact Sale</p>
                                        <ListCardHeaderRight>
                                            <AiOutlineHeart size="20" />
                                            <p>2</p>
                                        </ListCardHeaderRight>
                                    </ListCardHeader>

                                    <ListCardBodyContainer
                                        onClick={() =>
                                            Router.push('/asset/detail/325')
                                        }
                                    >
                                        <ListCardImgContainer>
                                            <img src="https://nftmania.io/uploads/cache/product/thumb-product_60ab25f01ac4a_350x.jpg" />
                                        </ListCardImgContainer>
                                        <ListCardBodyText>
                                            <ListCardBodyTextTop>
                                                <div>
                                                    <p>jejuislandbreak</p>
                                                    <h6>jejuislandbreak</h6>
                                                </div>
                                                <img src="https://nftmania.io/views/_layout/bootstrap/images/chainDecentral.png" />
                                            </ListCardBodyTextTop>
                                            <div>
                                                <p>Price</p>
                                                <h5>Nondisclosure</h5>
                                            </div>
                                            <ListCardBodyTextBottom>
                                                <AiOutlineClockCircle />
                                                2021-05-24
                                            </ListCardBodyTextBottom>
                                        </ListCardBodyText>
                                    </ListCardBodyContainer>
                                </ListCard>
                            </ListCardContainer>
                            <ListMoreButtonTextTop>more</ListMoreButtonTextTop>
                        </div>
                    </ListBodyContainer>
                </BodyLeftContainer>

                <div
                    style={{
                        flex: 1,
                        overflow: 'auto',
                        display: 'block',
                        height: 'calc(100vh - 174px)',
                    }}
                >
                    <div>
                        <BodyRightListDropContainer>
                            <div>
                                <AiFillStar size="20" />
                                <p>Status</p>
                            </div>
                            <BiChevronDown
                                size="40"
                                color="rgba(4, 17, 29, 0.5)"
                            />
                        </BodyRightListDropContainer>

                        <BodyRightListDropStatusUl>
                            <li>Direact Sale</li>
                            <li>Auction Sale</li>
                            <li>New</li>
                            <li>Has Offers</li>
                            <li>Highest Price Auction</li>
                            <li>Closed Auction</li>
                        </BodyRightListDropStatusUl>
                    </div>
                    <div>
                        <BodyRightListDropContainer>
                            <div>
                                <BsFillCollectionFill size="20" />
                                <p>Collections</p>
                            </div>
                            <BiChevronDown
                                size="40"
                                color="rgba(4, 17, 29, 0.5)"
                            />
                        </BodyRightListDropContainer>
                        <CollectionsFilterContainer>
                            <CollectionsFilter>
                                <BiSearchAlt2 size="30" />
                                <input placeholder="Filter" />
                            </CollectionsFilter>

                            <CollectionsFilterList>
                                <li>
                                    <img src="https://nftmania.io/uploads/collection/collection_608a9aaa57b98.png" />
                                    <label>
                                        <input type="checkbox" />
                                        SpecialForceNFT
                                    </label>
                                </li>
                            </CollectionsFilterList>
                        </CollectionsFilterContainer>
                    </div>
                    <div>
                        <BodyRightListDropContainer>
                            <div>
                                <BsFillTagFill size="20" />
                                <p>On Sale In</p>
                            </div>
                            <BiChevronDown
                                size="40"
                                color="rgba(4, 17, 29, 0.5)"
                            />
                        </BodyRightListDropContainer>
                        <CollectionsFilterContainer>
                            <CollectionsFilter>
                                <BiSearchAlt2 size="30" />
                                <input placeholder="Filter" />
                            </CollectionsFilter>

                            <CollectionsFilterList>
                                <li>
                                    <label>
                                        <input type="radio" defaultChecked />
                                        ALL
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input type="radio" />
                                        ETH
                                    </label>
                                </li>
                                {/*<li>
                                    <label>
                                        <input type="radio" />
                                        RUSH
                                    </label>
                                </li>*/}
                            </CollectionsFilterList>
                        </CollectionsFilterContainer>
                    </div>
                </div>
            </BodyContainer>
        </>
    );
};

export default Assetilist;
