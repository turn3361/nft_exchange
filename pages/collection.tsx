import { NextPage } from 'next';
import React, { useState } from 'react';
import { BsFillCollectionFill } from 'react-icons/bs';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import Layout from '../components/layout/NavBar';
import SignIn from '../components/SignIn';
import { RootStateInterface } from '../interfaces/RootState';
import { UserInfo } from '../interfaces/user/UserInfo.interface';
import Modal from 'react-modal';
import { MdClose } from 'react-icons/md';
import { AiOutlineFileImage } from 'react-icons/ai';
import Router from 'next/router';

const BodyContainer = styled.div`
    display: flex;
    flex-direction: row;
`;

const CollectionMenu = styled.div`
    padding: 32px 16px;
    display: flex;
    position: fixed;
    width: 300px;
    height: 100%;
    border-right: 1px solid rgb(229, 232, 235);

    a {
        width: 270px;
        padding: 12px 16px;
        display: flex;
        position: fixed;
        background-color: rgb(251, 253, 255);
        align-items: center;
        color: rgba(53, 56, 64, 0.75);
        svg {
            font-size: 24px;
            margin-right: 10px;
            font-weight: 500;
        }
        &:hover {
            background-color: rgb(229, 232, 235);
            color: rgb(47, 63, 78);
            cursor: pointer;
        }
    }
`;
const CollectionCardContainer = styled.div`
    margin-left: 300px;
    vertical-align: top;
    padding: 10px 30px 50px;
    display: flex;
    flex-direction: column;
    h1 {
        font-size: 26px;
        font-weight: 500;
        margin-top: 16px;
        margin-right: 32px;
        margin-bottom: 6px;
    }
    h2 {
        font-size: 15px;
        font-weight: 400;
        margin: 0 4px 12px 0;
        opacity: 0.6;
    }
`;
const CollectionCardList = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-items: center;
`;
const CreateCardContainer = styled.div`
    width: 250px;
    overflow: hidden;
    max-width: calc(50% - 38px);
    height: 300px;
    background-color: #fff;
    box-shadow: rgb(0 0 0 / 14%) 0px 2px 2px 0px,
        rgb(0 0 0 / 12%) 0px 1px 5px 0px, rgb(0 0 0 / 20%) 0px 3px 1px -2px;
    margin: 7.5px 16px 20px 0;
    border-radius: 5px;
    padding-top: 24px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;

    svg {
        color: rgb(187, 187, 187);
    }
    h4 {
        font-size: 16px;
        font-weight: 600;
        margin: 12px 0 24px;
    }
    button {
        width: 137px;
        height: 31px;
        margin-bottom: 25px;
        background-color: #e8632e;
        border: 1px solid #e8632e;
        border-radius: 5px;
        color: white;
        cursor: pointer;
    }
    &:hover {
        transition: all 0.3s ease-out 0s;
        box-shadow: rgb(47 63 78 / 50%) 0px 3px 1px 1px;
    }
`;

const CardContainer = styled(CreateCardContainer)`
    overflow: hidden;
    cursor: pointer;
    img {
        width: 100%;
        height: 100%;
        object-fit: corver;
        vertical-align: middle;
    }
    p {
        padding: 8px 12px;
        text-align: left;
        width: 100%;
        font-size: 14px;
    }
    &:hover {
        color: #0a58ca;
    }
`;

const ModalHeader = styled.div`
    display: flex;
    flex-shrink: 0;
    align-items: center;
    justify-content: space-between;
    padding: 1rem 1rem;
    border-bottom: 1px solid #dee2e6;
    border-top-left-radius: calc(0.3rem - 1px);
    border-top-right-radius: calc(0.3rem - 1px);
    h5 {
        font-size: 1.25rem;
        margin-bottom: 0;
        line-height: 1.5;
    }
    svg {
        padding: 0.5rem 0.5rem;
        margin: -0.5rem -0.5rem -0.5rem auto;
        cursor: pointer;
        box-sizing: content-box;
        padding: 0.25em 0.25em;
        color: #000;
        border: 0;
        border-radius: 0.25rem;
        opacity: 0.5;
    }
`;
const ModalBody = styled.div`
    padding: 20px 40px;
    position: relative;
    flex: 1 1 auto;
    h2 {
        font-size: 15px;
        font-weight: 400;
        margin: 3px 0 15px;
        text-align: center;
        line-height: 22px;
    }
    form {
        margin: 0;
        padding: 0;
    }
    label {
        font-size: 15px;
    }
    p {
        font-size: 15px;
        color: rgb(170, 170, 170);
    }
    textarea {
        margin-bottom: 20px;
        border-radius: 5px;
        border: 1px solid rgb(229, 232, 235);
        background: #fff;
        height: auto;
        padding: 16px 12px;
        resize: vertical;
        width: 100%;
        outline: none;
        min-height: 120px;
    }
`;
const ModalBodyFileSelector = styled.div`
    border-radius: 5px;
    border: 3px dashed rgb(204, 204, 204);
    cursor: pointer;
    height: 160px;
    margin-top: 6px;
    padding: 4px;
    width: 160px;
    margin-bottom: 20px;
    div {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 100%;
        height: 100%;
        position: relative;
    }
    img {
        width: 100%;
        height: 100%;
        object-fit: contain;
        position: absolute;
    }
    input {
        cursor: pointer;
        height: 100%;
        width: 100%;
        opacity: 0;
        position: absolute;
    }
    svg {
        color: rgb(204, 204, 204);
        font-size: 80px;
        pointer-events: none;
    }
`;
const ModalBodyNameSelect = styled.div`
    margin-bottom: 20px;
    background: #fff;
    border-radius: 5px;
    border: 1px solid rgb(229, 232, 235);
    display: flex;
    position: relative;
    input {
        background: transparent;
        border: none;
        flex: 1 0 0%;
        height: 48px;
        outline: none;
        padding-left: 12px;
        min-width: 0;
        margin: 0;
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }
`;
const ModalFooter = styled.div`
    display: flex;
    flex-wrap: wrap;
    flex-shrink: 0;
    align-items: center;
    justify-content: center;
    padding: 0.75rem;
    border-top: 1px solid #dee2e6;
    border-bottom-right-radius: calc(0.3rem - 1px);
    border-bottom-left-radius: calc(0.3rem - 1px);
    button {
        background-color: #e8632e;
        border: 1px solid #e8632e;
        cursor: pointer;
        padding: 0.5rem 1rem;
        font-size: 1.25rem;
        border-radius: 0.3rem;
        color: #fff;
        margin: 0.25rem;
        display: inline-block;
        font-weight: 400;
        line-height: 1.5;
        text-align: center;
        text-decoration: none;
        vertical-align: middle;
    }
`;
const Collection: NextPage = () => {
    const { isLogin } = useSelector(
        (state: RootStateInterface): UserInfo => state.userInfo,
    );
    const [modalIsOpen, setModalIsOpen] = useState(false);
    let subtitle: any;

    return (
        <>
            <Layout />
            <Modal
                isOpen={modalIsOpen}
                contentLabel="Example Modal"
                style={{
                    content: {
                        top: '50%',
                        left: '50%',
                        right: 'auto',
                        bottom: 'auto',
                        transform: 'translate(-50%, -50%)',
                        width: '598px',
                    },
                    overlay: {
                        zIndex: 1000,
                        background: 'rgba(100, 100, 100, 0.8)',
                    },
                }}
            >
                <ModalHeader ref={(_subtitle) => (subtitle = _subtitle)}>
                    <h5>Create your collection</h5>
                    <MdClose onClick={() => setModalIsOpen(false)} size="32" />
                </ModalHeader>
                <ModalBody>
                    <h2>You can change what you create below later.</h2>
                    <form>
                        <label>Logo</label>
                        <p>(350 x 350 recommended)</p>
                        <br />
                        <ModalBodyFileSelector>
                            <div>
                                <img src="/" />
                                <input type="file" />
                                <AiOutlineFileImage />
                            </div>
                        </ModalBodyFileSelector>
                        <br />
                        <label>Name *</label>
                        <ModalBodyNameSelect>
                            <input placeholder="e.g. NFTMANIA COLLECTIONS" />
                        </ModalBodyNameSelect>
                        <br />
                        <label>Description</label>
                        <p>0 of 1000 characters used</p>
                        <br />
                        <textarea placeholder="Provide a description for your store. Markdown syntax is supported." />
                    </form>
                </ModalBody>
                <ModalFooter>
                    <button onClick={() => setModalIsOpen(false)}>
                        Create
                    </button>
                </ModalFooter>
            </Modal>
            {!isLogin && <SignIn />}
            {!!isLogin && (
                <BodyContainer>
                    <CollectionMenu>
                        <a href="/collection">
                            <BsFillCollectionFill size="24" />
                            <p>My Collections</p>
                        </a>
                    </CollectionMenu>
                    <CollectionCardContainer>
                        <h1>My Collections</h1>
                        <h2>
                            You can create collections, upload digital
                            creations, organize commissions, and sell NFTs.
                        </h2>
                        <CollectionCardList>
                            <CreateCardContainer>
                                <BsFillCollectionFill size="40" />
                                <br />
                                <h4>Create new collection</h4>
                                <button onClick={() => setModalIsOpen(true)}>
                                    Create
                                </button>
                            </CreateCardContainer>
                            <CardContainer
                                onClick={() =>
                                    Router.push('/collection/info/325')
                                }
                            >
                                <img src="https://nftmania.io/uploads/collection/collection_60cc3ce85aabd.png" />
                                <p>Soung jehun</p>
                            </CardContainer>
                        </CollectionCardList>
                    </CollectionCardContainer>
                </BodyContainer>
            )}
        </>
    );
};

export default Collection;
